﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.tests.Mocks;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Emulation;

namespace ru.sazan.trader.tests.TraderBaseTests
{
    [TestClass]
    public class RestoreAtNotTradingTimePriceSignalBasedTests:TraderBaseInitializer
    {

        [TestInitialize]
        public void Setup()
        {
            Strategy strategy = new Strategy(6, "Strategy Description", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Signal>>().Count());
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Order>>().Count());
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Trade>>().Count());
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Position>>().Count());
            Assert.AreEqual(0, this.signalQueue.Count);
            Assert.AreEqual(0, this.orderQueue.Count);
        }

        [TestMethod]
        public void ignore_signals_when_backup_loaded_at_not_trading_time()
        {
            Transaction importSignals = new ImportSignalsTransaction(this.tradingData, ProjectRootFolderNameFactory.Make() + "\\signals-backup.txt");
            Transaction importOrders = new ImportOrdersTransaction((ObservableHashSetFactory)this.tradingData, ProjectRootFolderNameFactory.Make() + "\\orders-backup.txt");
            Transaction importTrades = new ImportTradesTransaction((ObservableHashSetFactory)this.tradingData, ProjectRootFolderNameFactory.Make() + "\\trades-backup.txt");

            importSignals.Execute();
            importOrders.Execute();
            importTrades.Execute();

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Signal>>().Count());
            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Order>>().Count());
            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Trade>>().Count());
            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Position>>().Count());
            Assert.AreEqual(0, this.signalQueue.Count);
            Assert.AreEqual(0, this.orderQueue.Count);
            Assert.AreEqual(0, ((MockOrderManager)this.orderManager).PlaceCounter);
        }
    }
}
