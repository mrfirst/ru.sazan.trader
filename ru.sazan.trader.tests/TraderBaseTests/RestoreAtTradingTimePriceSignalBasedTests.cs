﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.tests.Mocks;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Emulation;
using ru.sazan.trader.Handlers.StopLoss;
using ru.sazan.trader.Handlers.TakeProfit;

namespace ru.sazan.trader.tests.TraderBaseTests
{
    [TestClass]
    public class RestoreAtTradingTimePriceSignalBasedTests
    {
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private ObservableQueue<Order> orderQueue;
        private OrderManager orderManager;
        private TraderBase tb;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            this.signalQueue = new ObservableQueue<Signal>();
            this.orderQueue = new ObservableQueue<Order>();
            this.orderManager = new MockOrderManager();
            this.tb = new TraderBase(this.tradingData,
                this.signalQueue,
                this.orderQueue,
                this.orderManager,
                new AlwaysTimeToTradeSchedule(),
                new NullLogger());

            Strategy strategy = new Strategy(1, "Strategy Description", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            StopPointsSettings spSettings = new StopPointsSettings(strategy, 50, false);
            this.tradingData.Get<ICollection<StopPointsSettings>>().Add(spSettings);

            StopLossOrderSettings sloSettings = new StopLossOrderSettings(strategy, 100);
            this.tradingData.Get<ICollection<StopLossOrderSettings>>().Add(sloSettings);

            ProfitPointsSettings ppSettings = new ProfitPointsSettings(strategy, 100, false);
            this.tradingData.Get<ICollection<ProfitPointsSettings>>().Add(ppSettings);

            TakeProfitOrderSettings tpoSettings = new TakeProfitOrderSettings(strategy, 100);
            this.tradingData.Get<ICollection<TakeProfitOrderSettings>>().Add(tpoSettings);

            StrategyStopLossByPointsOnTick stopLossHandler =
                new StrategyStopLossByPointsOnTick(strategy, this.tradingData, this.signalQueue, new NullLogger());
            StrategyTakeProfitByPointsOnTick takeProfitHandler =
                new StrategyTakeProfitByPointsOnTick(strategy, this.tradingData, this.signalQueue, new NullLogger());

            PlaceStrategyStopLossByPointsOnTrade placeStopOnTradeHandler =
                new PlaceStrategyStopLossByPointsOnTrade(strategy, this.tradingData, this.signalQueue, new NullLogger());
            PlaceStrategyTakeProfitByPointsOnTrade placeTakeProfitOnTradeHandler =
                new PlaceStrategyTakeProfitByPointsOnTrade(strategy, this.tradingData, this.signalQueue, new NullLogger());


            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Signal>>().Count());
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Order>>().Count());
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Trade>>().Count());
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Position>>().Count());
            Assert.AreEqual(0, this.signalQueue.Count);
            Assert.AreEqual(0, this.orderQueue.Count);
        }

        [TestMethod]
        public void make_signals_when_backup_loaded_at_trading_time()
        {
            Transaction importSignals = new ImportSignalsTransaction(this.tradingData, ProjectRootFolderNameFactory.Make() + "\\signals-backup.txt");
            Transaction importOrders = new ImportOrdersTransaction((ObservableHashSetFactory)this.tradingData, ProjectRootFolderNameFactory.Make() + "\\orders-backup.txt");
            Transaction importTrades = new ImportTradesTransaction((ObservableHashSetFactory)this.tradingData, ProjectRootFolderNameFactory.Make() + "\\trades-backup.txt");

            importSignals.Execute();
            importOrders.Execute();
            importTrades.Execute();

            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Signal>>().Count());
            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Order>>().Count());
            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Trade>>().Count());
            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Position>>().Count());
            Assert.AreEqual(0, this.signalQueue.Count);
            Assert.AreEqual(0, this.orderQueue.Count);
            Assert.AreEqual(2, ((MockOrderManager)this.orderManager).PlaceCounter);
        }
    }
}
