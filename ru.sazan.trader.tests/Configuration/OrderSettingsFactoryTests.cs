﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Configuration;

namespace ru.sazan.trader.tests.Configuration
{
    [TestClass]
    public class OrderSettingsFactoryTests
    {
        [TestMethod]
        public void OrderSettingsFactory_test()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            string prefix = "RTSX";
            
            GenericFactory<OrderSettings> factory =
                new OrderSettingsFactory(strategy, prefix);

            OrderSettings sloSettings = factory.Make();

            Assert.AreEqual(strategy.Id, sloSettings.Id);
            Assert.AreEqual(strategy.Id, sloSettings.StrategyId);
            Assert.AreEqual(strategy, sloSettings.Strategy);
            Assert.AreEqual(86400, sloSettings.TimeToLive);
        }

        [TestMethod]
        public void OrderSettingsFactory_returns_null_for_nonexistent_prefix()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            string prefix = "RTSA";

            GenericFactory<OrderSettings> factory =
                new OrderSettingsFactory(strategy, prefix);

            Assert.IsNull(factory.Make());
        }
    }
}
