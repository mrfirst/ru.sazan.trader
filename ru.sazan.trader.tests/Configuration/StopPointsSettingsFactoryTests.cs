﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Configuration;

namespace ru.sazan.trader.tests.Configuration
{
    [TestClass]
    public class StopPointsSettingsFactoryTests
    {
        [TestMethod]
        public void StopPointsSettingsFactory_makes_settings_test()
        {
            Strategy strategy = new Strategy(1, "Description", "BP12345-RF-01", "RTS-12.13_FT", 1);

            string prefix = "RTSX";

            GenericFactory<StopPointsSettings> spSettingsFactory = new StopPointsSettingsFactory(strategy, prefix);

            StopPointsSettings spSettings = spSettingsFactory.Make();

            Assert.AreEqual(strategy.Id, spSettings.Id);
            Assert.AreEqual(strategy.Id, spSettings.StrategyId);
            Assert.AreEqual(strategy, spSettings.Strategy);
            Assert.AreEqual(500, spSettings.Points);
            Assert.AreEqual(false, spSettings.Trail);
        }

        [TestMethod]
        public void StopPointsSettingsFactory_makes_null_for_nonexistent_prefix()
        {
            Strategy strategy = new Strategy(1, "Description", "BP12345-RF-01", "RTS-12.13_FT", 1);

            string prefix = "RTSA";

            GenericFactory<StopPointsSettings> spSettingsFactory = new StopPointsSettingsFactory(strategy, prefix);

            Assert.IsNull(spSettingsFactory.Make());
        }
    }
}
