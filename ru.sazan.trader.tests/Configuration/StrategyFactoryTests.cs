﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Configuration;

namespace ru.sazan.trader.tests.Configuration
{
    [TestClass]
    public class StrategyFactoryTests
    {
        [TestMethod]
        public void StrategyFactory_tests()
        {
            string prefix = "RTSX";

            GenericFactory<Strategy> factory = new StrategyFactory(prefix);

            Strategy strategy = factory.Make();

            Assert.AreEqual(1, strategy.Id);
            Assert.AreEqual("Break out RTS", strategy.Description);
            Assert.AreEqual("BP12345-RF-01", strategy.Portfolio);
            Assert.AreEqual("RTS-9.13_FT", strategy.Symbol);
            Assert.AreEqual(15, strategy.Amount);
        }

        [TestMethod]
        public void StrategyFactory_makes_null_for_nonexistent_prefix_tests()
        {
            string prefix = "RTSD";

            GenericFactory<Strategy> factory = new StrategyFactory(prefix);

            Assert.IsNull(factory.Make());
        }
    }
}
