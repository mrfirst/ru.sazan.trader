﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Configuration;

namespace ru.sazan.trader.tests.Configuration
{
    [TestClass]
    public class DataProviderSettingsTests
    {
        [TestMethod]
        public void Make_DataProviderSettings_From_AppConfig()
        {
            DataProviderSettings settings = new DataProviderSettings();

            Assert.IsTrue(settings.ListenPortfolio);
            Assert.IsFalse(settings.ListenQuotes);
            Assert.IsFalse(settings.ListenBidAsk);
            Assert.IsTrue(settings.ListenTicks);
        }
    }
}
