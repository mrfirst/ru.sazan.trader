﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Configuration;

namespace ru.sazan.trader.tests.Configuration
{
    [TestClass]
    public class ProfitPointsSettingsFactoryTests
    {
        [TestMethod]
        public void ProfitPointsSettingsFactory_makes_settings_test()
        {
            Strategy strategy = new Strategy(1, "Description", "BP12345-RF-01", "RTS-12.13_FT", 1);

            string prefix = "RTSX";

            GenericFactory<ProfitPointsSettings> factory = new ProfitPointsSettingsFactory(strategy, prefix);

            ProfitPointsSettings ppSettings = factory.Make();

            Assert.AreEqual(strategy.Id, ppSettings.Id);
            Assert.AreEqual(strategy.Id, ppSettings.StrategyId);
            Assert.AreEqual(strategy, ppSettings.Strategy);
            Assert.AreEqual(1000, ppSettings.Points);
            Assert.AreEqual(false, ppSettings.Trail);
        }

        [TestMethod]
        public void ProfitPointsSettingsFactory_makes_null_for_nonexistent_prefix()
        {
            Strategy strategy = new Strategy(1, "Description", "BP12345-RF-01", "RTS-12.13_FT", 1);

            string prefix = "RTSA";

            GenericFactory<ProfitPointsSettings> factory = new ProfitPointsSettingsFactory(strategy, prefix);

            Assert.IsNull(factory.Make());
        }
    }
}
