﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Configuration;

namespace ru.sazan.trader.tests.Configuration
{
    [TestClass]
    public class StopLossOrderSettingsFactoryTests
    {
        [TestMethod]
        public void StopLossOrderSettingsFactory_test()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            string prefix = "RTSX";
            
            GenericFactory<StopLossOrderSettings> factory =
                new StopLossOrderSettingsFactory(strategy, prefix);

            StopLossOrderSettings sloSettings = factory.Make();

            Assert.AreEqual(strategy.Id, sloSettings.Id);
            Assert.AreEqual(strategy.Id, sloSettings.StrategyId);
            Assert.AreEqual(strategy, sloSettings.Strategy);
            Assert.AreEqual(3600, sloSettings.TimeToLive);
        }

        [TestMethod]
        public void StopLossOrderSettingsFactory_returns_null_for_nonexistent_prefix()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            string prefix = "RTSA";

            GenericFactory<StopLossOrderSettings> factory =
                new StopLossOrderSettingsFactory(strategy, prefix);

            Assert.IsNull(factory.Make());
        }
    }
}
