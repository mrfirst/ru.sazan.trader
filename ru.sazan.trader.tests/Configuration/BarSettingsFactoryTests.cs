﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Configuration;

namespace ru.sazan.trader.tests.Configuration
{
    [TestClass]
    public class BarSettingsFactoryTests
    {
        [TestMethod]
        public void BarSettingsFactory_test()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            string prefix = "RTSX";
            
            GenericFactory<BarSettings> factory =
                new BarSettingsFactory(strategy, prefix);

            BarSettings bSettings = factory.Make();

            Assert.IsTrue(bSettings.Id > 0);
            Assert.AreEqual(strategy.Id, bSettings.StrategyId);
            Assert.AreEqual(strategy, bSettings.Strategy);
            Assert.AreEqual("Si-12.13_FT", bSettings.Symbol);
            Assert.AreEqual(3600, bSettings.Interval);
            Assert.AreEqual(35, bSettings.Period);
        }

        [TestMethod]
        public void BarSettingsFactory_returns_null_for_nonexistent_prefix()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            string prefix = "RTSA";

            GenericFactory<BarSettings> factory =
                new BarSettingsFactory(strategy, prefix);

            Assert.IsNull(factory.Make());
        }
    }
}
