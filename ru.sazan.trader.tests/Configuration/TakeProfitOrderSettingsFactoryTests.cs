﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Configuration;

namespace ru.sazan.trader.tests.Configuration
{
    [TestClass]
    public class TakeProfitOrderSettingsFactoryTests
    {
        [TestMethod]
        public void TakeProfitOrderSettingsFactory_make_test()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            string prefix = "RTSX";

            GenericFactory<TakeProfitOrderSettings> factory =
                new TakeProfitOrderSettingsFactory(strategy, prefix);

            TakeProfitOrderSettings tpoSettings = factory.Make();
            Assert.AreEqual(strategy.Id, tpoSettings.Id);
            Assert.AreEqual(strategy.Id, tpoSettings.StrategyId);
            Assert.AreEqual(strategy, tpoSettings.Strategy);
            Assert.AreEqual(1900, tpoSettings.TimeToLive);
        }

        [TestMethod]
        public void TakeProfitOrderSettingsFactory_returns_null_for_nonexistent_prefix()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            string prefix = "RTSA";

            GenericFactory<TakeProfitOrderSettings> factory =
                new TakeProfitOrderSettingsFactory(strategy, prefix);

            Assert.IsNull(factory.Make());
        }
    }
}
