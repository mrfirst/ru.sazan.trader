using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Net;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.tests.Net
{
	[TestClass]
	public class ManagementProtocolMessagesTests
	{
		[TestMethod]
		public void ConnectMessage()
		{
			Guid srcAddr = Guid.NewGuid();
			Guid dstAddr = Guid.NewGuid();
			
			Message connectMsg = 
				new ConnectMessage(srcAddr, dstAddr);

			Assert.AreEqual(srcAddr, new Guid(connectMsg.GetSourceAddressBytes()));
			Assert.AreEqual(dstAddr, new Guid(connectMsg.GetDestinationAddressBytes()));
			Assert.AreEqual("connect", connectMsg.GetContentBytes().AsString());
		}

		[TestMethod]
		public void DisconnectMessage()
		{
			Guid srcAddr = Guid.NewGuid();
			Guid dstAddr = Guid.NewGuid();

			Message msg = 
				new DisconnectMessage(srcAddr, dstAddr);

			Assert.AreEqual(srcAddr, new Guid(msg.GetSourceAddressBytes()));
			Assert.AreEqual(dstAddr, new Guid(msg.GetDestinationAddressBytes()));

			Assert.AreEqual("disconnect", msg.GetContentBytes().AsString());
		}

		[TestMethod]
		public void KeepaliveMessage()
		{
			Guid srcAddr = Guid.NewGuid();
			Guid dstAddr = Guid.NewGuid();

			Message msg = 
				new KeepaliveMessage(srcAddr, dstAddr);

			Assert.AreEqual(srcAddr, new Guid(msg.GetSourceAddressBytes()));
			Assert.AreEqual(dstAddr, new Guid(msg.GetDestinationAddressBytes()));

			Assert.AreEqual("ping", msg.GetContentBytes().AsString());
		}

		[TestMethod]
		public void OutOfServiceMessage()
		{
			Guid srcAddr = Guid.NewGuid();
			Guid dstAddr = Guid.NewGuid();
			
			Message msg = 
				new OutOfServiceMessage(srcAddr, dstAddr);

			Assert.AreEqual(srcAddr, new Guid(msg.GetSourceAddressBytes()));
			Assert.AreEqual(dstAddr, new Guid(msg.GetDestinationAddressBytes()));
			Assert.AreEqual("out of service", msg.GetContentBytes().AsString());
		}

		[TestMethod]
		public void InServiceMessage()
		{
			Guid srcAddr = Guid.NewGuid();
			Guid dstAddr = Guid.NewGuid();

			Message msg = 
				new InServiceMessage(srcAddr, dstAddr);

			Assert.AreEqual(srcAddr, new Guid(msg.GetSourceAddressBytes()));
			Assert.AreEqual(dstAddr, new Guid(msg.GetDestinationAddressBytes()));
			Assert.AreEqual("in service", msg.GetContentBytes().AsString());
		}
	}
}
