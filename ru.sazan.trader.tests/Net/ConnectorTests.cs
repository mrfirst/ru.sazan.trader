﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.tests.Mocks;
using ru.sazan.trader.Net;

namespace ru.sazan.trader.tests.Net
{
    [TestClass]
    public class ConnectorTests
    {
        private Connector connector;

        [TestInitialize]
        public void Setup()
        {
            this.connector = new MockConnector();
        }

        [TestMethod]
        public void Connection_Lost()
        {
            Assert.IsFalse(this.connector.IsConnected);

            this.connector.Connect();

            Assert.IsTrue(this.connector.IsConnected);

            EmulateDisconnect();

            Assert.IsFalse(this.connector.IsConnected);
        }

        private void EmulateDisconnect()
        {
            MockConnector mc = (MockConnector)this.connector;

            mc.EmulateDisconnect();
        }
    }
}
