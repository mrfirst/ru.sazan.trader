﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.tests.Utility
{
    [TestClass]
    public class ImportBarsTransactionTests
    {
        private DataContext marketData;
        
        [TestInitialize]
        public void Setup()
        {
            this.marketData = new TradingDataContext();
        }

        [TestMethod]
        public void import_bars_when_file_exists()
        {
            string path = String.Concat(ProjectRootFolderNameFactory.Make(), "\\SPFB.RTS_130807_130807.txt");            
            
            Assert.AreEqual(0, this.marketData.Get<ObservableCollection<Bar>>().Count);

            Transaction import = new ImportBarsTransaction(this.marketData.Get<ObservableCollection<Bar>>(), path);
            import.Execute();

            Assert.AreEqual(7, this.marketData.Get<ObservableCollection<Bar>>().Count);
        }

        [TestMethod]
        public void do_nothing_when_file_not_exists()
        {
            Transaction import = new ImportBarsTransaction(this.marketData.Get<ObservableCollection<Bar>>(), "no-such-file.txt");
            import.Execute();

            Assert.AreEqual(0, this.marketData.Get<ObservableCollection<Bar>>().Count);
        }
    }
}
