﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.tests.Utility
{
    [TestClass]
    public class TextFileStringListFactoryTests
    {
        [TestMethod]
        public void TextFileStringListFactory_test()
        {
            GenericFactory<IEnumerable<string>> logReader = new TextFileStringListFactory(String.Concat(ProjectRootFolderNameFactory.Make(), "\\logfile.txt"));

            IEnumerable<string> log = logReader.Make();

            Assert.AreEqual(10, log.Count());
        }
    }
}
