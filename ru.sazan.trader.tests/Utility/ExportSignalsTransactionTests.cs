﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using System.IO;

namespace ru.sazan.trader.tests.Utility
{
    [TestClass]
    public class ExportSignalsTransactionTests
    {
        private DataContext tradingData;
        private string ef;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            this.ef = String.Concat(ProjectRootFolderNameFactory.Make(), "\\export-signals.txt");
        }

        [TestCleanup]
        public void TearDown()
        {
            if (File.Exists(ef))
                File.Delete(ef);
        }

        [TestMethod]
        public void export_three_signals()
        {
            Strategy st1 = new Strategy(1, "First strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(st1);

            Strategy st2 = new Strategy(2, "Second strategy", "BP12345-RF-01", "RTS-9.13_FT", 8);
            this.tradingData.Get<ICollection<Strategy>>().Add(st2);

            Strategy st3 = new Strategy(3, "Third strategy", "BP12345-RF-01", "RTS-9.13_FT", 5);
            this.tradingData.Get<ICollection<Strategy>>().Add(st3);

            Signal s1 = new Signal(st1, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(s1);

            Signal s2 = new Signal(st2, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Limit, 150000, 0, 150000);
            this.tradingData.Get<ICollection<Signal>>().Add(s2);

            Signal s3 = new Signal(st3, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Stop, 150000, 150000, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(s3);

            Transaction export = new ExportSignalsTransaction((ObservableHashSetFactory)this.tradingData, this.ef);
            export.Execute();            
            Assert.IsTrue(File.Exists(this.ef));

            this.tradingData.Get<ICollection<Signal>>().Clear();
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Signal>>().Count());

            Transaction import = new ImportSignalsTransaction(this.tradingData, this.ef);
            import.Execute();
            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Signal>>().Count());
        }

        [TestMethod]
        public void ExportSignalsTransaction_do_nothing_when_no_signals_exists()
        {
            Strategy st1 = new Strategy(1, "First strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(st1);

            Strategy st2 = new Strategy(2, "Second strategy", "BP12345-RF-01", "RTS-9.13_FT", 8);
            this.tradingData.Get<ICollection<Strategy>>().Add(st2);

            Strategy st3 = new Strategy(3, "Third strategy", "BP12345-RF-01", "RTS-9.13_FT", 5);
            this.tradingData.Get<ICollection<Strategy>>().Add(st3);

            Transaction export = new ExportSignalsTransaction((ObservableHashSetFactory)this.tradingData, this.ef);
            export.Execute();
            Assert.IsFalse(File.Exists(this.ef));
        }
    }
}
