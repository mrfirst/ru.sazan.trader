﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.tests.Utility
{
    [TestClass]
    public class StructFactoryTests
    {
        [TestMethod]
        public void ConvertStringToNullableInt32()
        {
            string source = "100";

            Assert.AreEqual(100, StructFactory.MakeNullable<int>(source));
        }

        [TestMethod]
        public void ConvertStringToInt32()
        {
            string source = "100";

            Assert.AreEqual(100, StructFactory.Make<int>(source));
        }

        [TestMethod]
        public void ConvertStringToDecimal()
        {
            string source = "100,00";

            Assert.AreEqual(100m, StructFactory.Make<Decimal>(source));
        }

        [TestMethod]
        public void ConvertStringToNullableDecimal()
        {
            string source = "100,00";

            Assert.AreEqual(100m, StructFactory.MakeNullable<Decimal>(source));
        }

        [TestMethod]
        public void ConvertStringToFloat()
        {
            string source = "100,00";

            Assert.AreEqual(100F, StructFactory.Make<float>(source));
        }

        [TestMethod]
        public void ConvertStringToNullableFloat()
        {
            string source = "100,00";

            Assert.AreEqual(100F, StructFactory.MakeNullable<float>(source));
        }

        [TestMethod]
        public void ConvertStringToGuid()
        {
            string source = "88888888888888888888888888888888";

            Assert.AreEqual(Guid.Parse(source), StructFactory.Make<Guid>(source));
        }

        [TestMethod]
        public void ConvertStringToNullableGuid()
        {
            string source = "88888888888888888888888888888888";

            Assert.AreEqual(Guid.Parse(source), StructFactory.MakeNullable<Guid>(source));
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Make_Int_From_EmptyString_Raise_An_Argument_Exception()
        {
            StructFactory.Make<int>(" ");
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Make_NullableGuid_From_EmptyString_Raise_An_Argument_Exception()
        {
            StructFactory.MakeNullable<Guid>("");
        }
    }
}
