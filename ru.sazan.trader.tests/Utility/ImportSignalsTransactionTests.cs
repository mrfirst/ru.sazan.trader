﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.tests.Utility
{
    [TestClass]
    public class ImportSignalsTransactionTests
    {
        private DataContext tradingData;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
        }

        [TestMethod]
        public void import_signals()
        {
            Strategy strategy = new Strategy(1, "First strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            Strategy strategy1 = new Strategy(2, "Second strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy1);

            Strategy strategy2 = new Strategy(3, "Third strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy2);

            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Signal>>().Count());

            string ef = String.Concat(ProjectRootFolderNameFactory.Make(), "\\signals.txt");
            Transaction import = new ImportSignalsTransaction(this.tradingData, ef);

            import.Execute();

            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Signal>>().Count());
        }

        [TestMethod]
        public void import_signal_only_for_existent_strategy()
        {
            Strategy strategy = new Strategy(1, "First strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Signal>>().Count());

            string ef = String.Concat(ProjectRootFolderNameFactory.Make(), "\\signals.txt");
            Transaction import = new ImportSignalsTransaction(this.tradingData, ef);

            import.Execute();

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Signal>>().Count());
        }
    }
}
