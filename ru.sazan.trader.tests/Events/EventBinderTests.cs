﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Events;
using ru.sazan.trader.tests.Mocks;

namespace ru.sazan.trader.tests.Events
{
    [TestClass]
    public class EventBinderTests
    {
        [TestMethod]
        public void EventBinder_Bind_And_Unbind_An_Events()
        {
            GenericBinder binder = new MockEventBinder();

            MockEventBinder mockEventBinder = (MockEventBinder)binder;

            binder.Bind<MockEvent>(MockEventHandler);
            Assert.AreEqual(1, mockEventBinder.BindedEventsCounter);

            binder.Bind<MockEvent>(MockEventHandler);
            Assert.AreEqual(2, mockEventBinder.BindedEventsCounter);



            binder.Unbind<MockEvent>(MockEventHandler);
            Assert.AreEqual(1, mockEventBinder.BindedEventsCounter);

            binder.Unbind<MockEvent>(MockEventHandler);
            Assert.AreEqual(0, mockEventBinder.BindedEventsCounter);
        }

        public void MockEventHandler()
        {
        }
    }
}
