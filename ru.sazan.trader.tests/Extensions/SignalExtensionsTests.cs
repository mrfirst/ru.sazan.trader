﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class SignalExtensionsTests
    {
        private Strategy strategy;

        [TestInitialize]
        public void Setup()
        {
            this.strategy = new Strategy(1, "description", "BP12345-RF-01", "RTS-9.13_FT", 10);
        }

        [TestMethod]
        public void Signal_to_buy_GetSignedAmount_test()
        {
            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);

            Assert.AreEqual(10, signal.GetSignedAmount());
        }

        [TestMethod]
        public void Signal_to_sell_GetSignedAmount_test()
        {
            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Market, 150000, 0, 0);

            Assert.AreEqual(-10, signal.GetSignedAmount());
        }
    }
}
