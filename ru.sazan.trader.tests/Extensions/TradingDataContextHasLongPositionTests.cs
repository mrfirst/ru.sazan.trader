﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Models;
using ru.sazan.trader.Emulation;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class TradingDataContextHasLongPositionTests
    {
        private DataContext tradingData;
        private Strategy strategy;

        [TestInitialize]
        public void Setup() 
        {
            this.tradingData = new TradingDataContext();
            
            this.strategy = new Strategy(1, "Description", "BP12345-RF-01", "RTS-3.14_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(this.strategy);
        }

        [TestMethod]
        public void TradingDataContext_has_no_long_position_when_no_any_position_exists_test()
        {
            Assert.IsFalse(this.tradingData.HasLongPosition(this.strategy));
        }

        [TestMethod]
        public void TradingDataContext_has_no_long_position_when_there_is_no_trades_for_strategy_test()
        {
            Signal signal = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Market, 150000, 0, 0);
            this.tradingData.AddSignalAndItsOrder(signal);

            Assert.IsFalse(this.tradingData.HasLongPosition(this.strategy));
        }

        [TestMethod]
        public void TradingDataContext_has_long_position_test()
        {
            Signal signal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.tradingData.AddSignalAndItsOrderAndTrade(signal);

            Assert.IsTrue(this.tradingData.HasLongPosition(this.strategy));
        }
    }
}
