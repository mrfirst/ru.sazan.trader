﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Emulation;
using ru.sazan.trader.Handlers.StopLoss;
using ru.sazan.trader.Handlers.TakeProfit;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class TradingDataContextGetMoveRequestsTests:TraderBaseInitializer
    {
        private Strategy strategy;

        [TestInitialize]
        public void Setup()
        {
            this.strategy = this.tradingData.Get<IEnumerable<Strategy>>().Single(s => s.Id == 1);

            StopPointsSettings spSettings = new StopPointsSettings(this.strategy, 100, false);
            this.tradingData.Get<ObservableHashSet<StopPointsSettings>>().Add(spSettings);

            StopLossOrderSettings slSettings = new StopLossOrderSettings(this.strategy, 3600);
            this.tradingData.Get<ObservableHashSet<StopLossOrderSettings>>().Add(slSettings);

            StrategyStopLossByPointsOnTick stopLossHandler =
                new StrategyStopLossByPointsOnTick(strategy, this.tradingData, this.signalQueue, new NullLogger());
            StrategyTakeProfitByPointsOnTick takeProfitHandler =
                new StrategyTakeProfitByPointsOnTick(strategy, this.tradingData, this.signalQueue, new NullLogger());

            PlaceStrategyStopLossByPointsOnTrade placeStopOnTradeHandler =
                new PlaceStrategyStopLossByPointsOnTrade(strategy, this.tradingData, this.signalQueue, new NullLogger());
            PlaceStrategyTakeProfitByPointsOnTrade placeTakeProfitOnTradeHandler =
                new PlaceStrategyTakeProfitByPointsOnTrade(strategy, this.tradingData, this.signalQueue, new NullLogger());

        }

        [TestMethod]
        public void TradingDataContext_GetMoveRequest_returns_empty_collection_when_no_any_requests_exists_test()
        {
            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            EmulateTradeFor(signal);

            Assert.AreEqual(2, this.tradingData.Get<IEnumerable<Signal>>().Count());

            Order slOrder = this.tradingData.GetCloseOrders(this.strategy).Last();

            Assert.AreEqual(0, this.tradingData.GetMoveRequests(slOrder).Count());
        }

        [TestMethod]
        public void TradingDataContext_GetMoveRequests_for_order_test()
        {
            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            EmulateTradeFor(signal);

            Assert.AreEqual(2, this.tradingData.Get<IEnumerable<Signal>>().Count());

            Order slOrder = this.tradingData.GetCloseOrders(this.strategy).Last();
            Assert.AreEqual(OrderType.Stop, slOrder.OrderType);
            Assert.AreEqual(149900, slOrder.Stop);

            OrderMoveRequest slMoveReq = new OrderMoveRequest(slOrder, 0, 150000, "Trail stop");
            this.tradingData.Get<ObservableCollection<OrderMoveRequest>>().Add(slMoveReq);

            OrderMoveRequest slMoveReq2 = new OrderMoveRequest(slOrder, 0, 150100, "Trail stop");
            this.tradingData.Get<ObservableCollection<OrderMoveRequest>>().Add(slMoveReq2);

            Assert.AreEqual(2, this.tradingData.GetMoveRequests(slOrder).Count());
        }

        [TestMethod]
        public void TradingDataContext_GetMoveRequests_ignore_other_orders_test()
        {
            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            EmulateTradeFor(signal);

            Assert.AreEqual(2, this.tradingData.Get<IEnumerable<Signal>>().Count());

            Order slOrder = this.tradingData.GetCloseOrders(this.strategy).Last();
            Assert.AreEqual(OrderType.Stop, slOrder.OrderType);
            Assert.AreEqual(149900, slOrder.Stop);

            OrderMoveRequest slMoveReq = new OrderMoveRequest(slOrder, 0, 150000, "Trail stop");
            this.tradingData.Get<ObservableCollection<OrderMoveRequest>>().Add(slMoveReq);

            OrderMoveRequest slMoveReq2 = new OrderMoveRequest(slOrder, 0, 150100, "Trail stop");
            this.tradingData.Get<ObservableCollection<OrderMoveRequest>>().Add(slMoveReq2);

            Signal tpSignal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Limit, 151000, 0, 151000);
            this.signalQueue.Enqueue(tpSignal);

            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Order>>().Count());

            Order tpOrder = this.tradingData.Get<IEnumerable<Order>>().Last();
            Assert.AreEqual(OrderType.Limit, tpOrder.OrderType);

            OrderMoveRequest tpMoveReq = new OrderMoveRequest(tpOrder, 152000, 0, "Trail profit");
            this.tradingData.Get<ObservableCollection<OrderMoveRequest>>().Add(tpMoveReq);

            Assert.AreEqual(2, this.tradingData.GetMoveRequests(slOrder).Count());
        }
    }
}
