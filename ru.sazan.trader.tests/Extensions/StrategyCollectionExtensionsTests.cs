﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Configuration;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class StrategyCollectionExtensionsTests
    {
        private DataContext tradingData;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            
            AddStrategies();
        }

        private void AddStrategies()
        {
            this.tradingData.Get<ICollection<Strategy>>().Add(new Strategy(1, "First", "BP12345-RF-01", "RTS-9.13_FT", 10));
            this.tradingData.Get<ICollection<Strategy>>().Add(new Strategy(2, "Second", "BP12345-RF-02", "RTS-9.13_FT", 10));
            this.tradingData.Get<ICollection<Strategy>>().Add(new Strategy(3, "Third", "BP12345-RF-01", "RTS-9.13_FT", 10));
            Assert.AreEqual(3, this.tradingData.Get<ICollection<Strategy>>().Count);
        }

        [TestMethod]
        public void StrategyCollection_GetAmount_test()
        {
            Assert.AreEqual(20, this.tradingData.Get<ICollection<Strategy>>().GetAmount("BP12345-RF-01", "RTS-9.13_FT"));
            Assert.AreEqual(10, this.tradingData.Get<ICollection<Strategy>>().GetAmount("BP12345-RF-02", "RTS-9.13_FT"));
        }
    }
}
