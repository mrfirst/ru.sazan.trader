﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.tests.Mocks;

namespace ru.sazan.trader.tests.Data
{
    [TestClass]
    public class HistoryDataProviderTests
    {
        private DataContext tradingData;
        private HistoryDataProvider historyDataProvider;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            this.historyDataProvider = new FakeHistoryDataProvider(this.tradingData);

            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Tick>>().Where(t => t.Symbol == "RTS-6.14").Count());
        }

        [TestMethod]
        public void HistoryDataProvider_send_tick_request_test()
        {
            HistoryDataRequest tickRequest =
                new TickHistoryDataRequest("RTS-6.14", 10, DateTime.Now);

            this.historyDataProvider.Send(tickRequest);

            Assert.AreEqual(10, this.tradingData.Get<IEnumerable<Tick>>().Where(t => t.Symbol == "RTS-6.14").Count());
        }
    }
}
