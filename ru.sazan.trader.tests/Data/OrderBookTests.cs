﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.tests.Data
{
    [TestClass]
    public class OrderBookTests
    {
        [TestMethod]
        public void OrderBook_is_singleton()
        {
            OrderBookContext storage = OrderBook.Instance;
            OrderBookContext anotherStorage = OrderBook.Instance;

            Assert.AreSame(storage, anotherStorage);
        }
    }
}
