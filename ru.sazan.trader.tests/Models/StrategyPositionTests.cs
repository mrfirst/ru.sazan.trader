﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class StrategyPositionTests
    {
        private Strategy strategy;

        private StrategyPosition strategyPosition;

        [TestInitialize]
        public void SetUp()
        {
            this.strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-3.14_FT", 10);

            this.strategyPosition = new StrategyPosition(strategy);
        }

        [TestMethod]
        public void StrategyPosition_default_constructor_test()
        {
            Assert.IsInstanceOfType(strategyPosition, typeof(Identified));
            Assert.AreEqual(strategy.Id, strategyPosition.Id);
            Assert.AreEqual(strategy, strategyPosition.Strategy);
            Assert.AreEqual(strategy.Id, strategyPosition.StrategyId);
            Assert.AreEqual(0, strategyPosition.Trades.Count());
            Assert.AreEqual(0, strategyPosition.Amount);
            Assert.AreEqual(0, strategyPosition.Sum);
        }
    }
}
