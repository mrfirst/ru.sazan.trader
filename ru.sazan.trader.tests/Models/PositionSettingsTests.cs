﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class PositionSettingsTests
    {
        [TestMethod]
        public void PositionSettings_constructor_tests()
        {
            Strategy strategy = new Strategy(1, "Strategy sample", "BP12345-RF-01", "RTS-12.13_FT", 1);

            PositionSettings ps = new PositionSettings(strategy, PositionType.Long);

            Assert.AreEqual(strategy.Id, ps.Id);
            Assert.AreEqual(strategy.Id, ps.StrategyId);
            Assert.AreEqual(strategy, ps.Strategy);
            Assert.AreEqual(PositionType.Long, ps.PositionType);
        }
    }
}
