﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class PointsSettingsTests
    {
        [TestMethod]
        public void ClosePointsSettings_constructor_test()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-9.13_FT", 8);

            PointsSettings settings = new PointsSettings(strategy, 350, true);

            Assert.AreEqual(strategy.Id, settings.Id);
            Assert.AreSame(strategy, settings.Strategy);
            Assert.AreEqual(strategy.Id, settings.StrategyId);
            Assert.AreEqual(350, settings.Points);
            Assert.IsTrue(settings.Trail);
        }
    }
}
