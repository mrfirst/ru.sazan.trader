﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class StrategyVolumeChangeStepTests
    {
        [TestMethod]
        public void StrategyVolumeChangeStep_constructor_test()
        {
            double stepValue = 10;

            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "SBRF-3.14_FT", 100);

            StrategyVolumeChangeStep step = new StrategyVolumeChangeStep(strategy, stepValue);

            Assert.AreEqual(stepValue, step.Amount);
            Assert.AreEqual(strategy.Id, step.Id);
            Assert.AreEqual(strategy.Id, step.StrategyId);
            Assert.AreEqual(strategy, step.Strategy);
        }
    }
}
