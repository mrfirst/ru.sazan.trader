﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class TakeProfitOrderSettingsTests
    {
        [TestMethod]
        public void TakeProfitOrderSettings_constructor_test()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);

            TakeProfitOrderSettings tp = new TakeProfitOrderSettings(strategy, 3600);

            Assert.IsInstanceOfType(tp, typeof(OrderSettings));
            Assert.AreEqual(strategy.Id, tp.Id);
            Assert.AreEqual(strategy.Id, tp.StrategyId);
            Assert.AreEqual(strategy, tp.Strategy);
            Assert.AreEqual(3600, tp.TimeToLive);
        }
    }
}
