﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class SMASettingsTests
    {
        [TestMethod]
        public void SMASettings_constructor_test()
        {
            Strategy strategy = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 10);
            SMASettings settings = new SMASettings(strategy, 5, 10);

            Assert.IsTrue(settings.Id > 0);
            Assert.AreEqual(strategy, settings.Strategy);
            Assert.AreEqual(strategy.Id, settings.StrategyId);
            Assert.AreEqual(5, settings.FastPeriod);
            Assert.AreEqual(10, settings.SlowPeriod);
        }
    }
}
