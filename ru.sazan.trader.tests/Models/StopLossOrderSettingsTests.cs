﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class StopLossOrderSettingsTests
    {
        [TestMethod]
        public void StopLossOrderSettings_constructor_test()
        {
            Strategy strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);

            StopLossOrderSettings sl = new StopLossOrderSettings(strategy, 3600);

            Assert.IsInstanceOfType(sl, typeof(OrderSettings));
            Assert.AreEqual(strategy.Id, sl.Id);
            Assert.AreEqual(strategy.Id, sl.StrategyId);
            Assert.AreEqual(strategy, sl.Strategy);
            Assert.AreEqual(3600, sl.TimeToLive);
        }
    }
}
