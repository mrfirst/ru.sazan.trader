﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class ProfitPointsSettingsTests
    {
        [TestMethod]
        public void TakeProfitSettings_constructor_test()
        {
            Strategy strategy = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 10);
            double points = 100;
            bool trail = false;

            ProfitPointsSettings settings = new ProfitPointsSettings(strategy, points, trail);

            Assert.AreEqual(strategy.Id, settings.Id);
            Assert.AreEqual(strategy.Id, settings.StrategyId);
            Assert.AreEqual(strategy, settings.Strategy);
            Assert.AreEqual(points, settings.Points);
            Assert.AreEqual(trail, settings.Trail);
            Assert.IsInstanceOfType(settings, typeof(PointsSettings));
        }
    }
}
