﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class OrderSettingsTests
    {
        [TestMethod]
        public void OrderSettings_constructor_test()
        {
            Strategy strategy = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 10);
            int timeToLiveSeconds = 60;

            OrderSettings orderSettings = new OrderSettings(strategy, timeToLiveSeconds);

            Assert.IsTrue(orderSettings.Id > 0);
            Assert.AreEqual(strategy, orderSettings.Strategy);
            Assert.AreEqual(strategy.Id, orderSettings.StrategyId);
            Assert.AreEqual(timeToLiveSeconds, orderSettings.TimeToLive);
        }
    }
}
