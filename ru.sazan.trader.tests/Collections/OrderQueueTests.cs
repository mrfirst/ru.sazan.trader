﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Collections
{
    [TestClass]
    public class OrderQueueTests
    {
        [TestMethod]
        public void OrderQueue_Instance_Is_ObservableQueue()
        {
            Assert.IsInstanceOfType(OrderQueue.Instance, typeof(ObservableQueue<Order>));
        }

        [TestMethod]
        public void OrderQueue_Instance()
        {
            Assert.IsNotNull(OrderQueue.Instance);
        }

        [TestMethod]
        public void OrderQueue_Is_Singleton()
        {
            OrderQueue q = OrderQueue.Instance;
            OrderQueue q2 = OrderQueue.Instance;

            Assert.AreSame(q, q2);
        }
    }
}
