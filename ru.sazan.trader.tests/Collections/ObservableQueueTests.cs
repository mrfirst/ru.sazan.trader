﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.tests.Mocks;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.tests.Collections
{
    [TestClass]
    public class ObservableQueueTests
    {

        private ObservableQueue<string> queue;
        private MockQueueObserver observer;

        [TestInitialize]
        public void SetUp()
        {
            this.queue = new ObservableQueue<string>();

            this.observer = new MockQueueObserver(this.queue);

            this.queue.RegisterObserver(this.observer);
        }

        [TestMethod]
        public void ObservableQueue_Notify_Observer()
        {
            Assert.AreEqual("No data", this.observer.Data);

            this.queue.Enqueue("New data");

            Assert.AreEqual("New data", this.observer.Data);
        }
    }
}
