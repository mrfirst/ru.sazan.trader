﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Events;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.tests.Collections
{
    public class OrderHashSetObserver : GenericObserver<Order>
    {
        public int Count { get; set; }

        public OrderHashSetObserver()
        {
            this.Count = 0;
        }

        public void Update(Order item)
        {
            this.Count++;
        }
    }

    [TestClass]
    public class ObservableHashSetTests
    {
        [TestMethod]
        public void ObservableHashSet_test()
        {
            OrderHashSetObserver o = new OrderHashSetObserver();
            ObservableHashSet<Order> hs = new ObservableHashSet<Order>();
            hs.RegisterObserver(o);

            Assert.AreEqual(0, o.Count);
            hs.Add(new Order());

            Assert.AreEqual(1, o.Count);

        }
    }
}
