﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Collections
{
    [TestClass]
    public class TradeSignalQueueTests
    {
        [TestMethod]
        public void TradeSignalQueue_Instance_Is_ObservableQueue()
        {
            Assert.IsInstanceOfType(SignalQueue.Instance, typeof(ObservableQueue<Signal>));
        }

        [TestMethod]
        public void TradeSignalQueue_Instance()
        {
            Assert.IsNotNull(SignalQueue.Instance);
        }

        [TestMethod]
        public void TradeSignalQueue_Is_Singleton()
        {
            SignalQueue q = SignalQueue.Instance;
            SignalQueue q2 = SignalQueue.Instance;

            Assert.AreSame(q, q2);
        }
    }
}
