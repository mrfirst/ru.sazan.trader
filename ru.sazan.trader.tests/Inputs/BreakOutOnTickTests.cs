﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.tests.Mocks;
using ru.sazan.trader.Models;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Configuration;
using ru.sazan.trader.Inputs;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Emulation;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.tests.Inputs
{
    [TestClass]
    public class BreakOutOnTickTests
    {
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private Strategy strategy;
        private BarSettings barSettings;
        private BreakOutOnTick handler;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            this.signalQueue = new ObservableQueue<Signal>();

            this.strategy = new Strategy(1, "Break out", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(this.strategy);

            this.barSettings = new BarSettings(this.strategy, "RTS-9.13_FT", 3600, 19);
            this.tradingData.Get<ICollection<BarSettings>>().Add(this.barSettings);

            this.handler =
                new BreakOutOnTick(this.strategy, this.tradingData, this.signalQueue, new NullLogger());

            FillTradingDataWithBars();

            Assert.AreEqual(0, this.signalQueue.Count);
            Assert.IsFalse(this.tradingData.PositionExists(this.strategy));
            Assert.IsFalse(this.tradingData.UnfilledExists(this.strategy));
        }

        private void FillTradingDataWithBars()
        {
            for (int i = 0; i < this.barSettings.Period; i++ )
                this.tradingData.Get<ObservableCollection<Bar>>().Add(
                    new Bar(this.strategy.Symbol,
                        this.barSettings.Interval,
                        BrokerDateTime.Make(DateTime.Now),
                        i,
                        i,
                        i,
                        i,
                        i));
        }

        [TestMethod]
        public void do_nothing_when_tick_has_other_symbol_test()
        {
            this.tradingData.Get<ObservableCollection<Tick>>().Add(
                new Tick("Si", DateTime.Now, TradeAction.Buy, 1000000, 1));

            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void do_nothing_when_tick_price_has_no_break_out_value_test()
        {
            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick { Symbol = "RTS-9.13_FT", Price = 5 });

            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void do_nothing_on_break_to_high_when_long_position_exists_for_strategy_test()
        {
            Signal openSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 0, 0, 0);
            this.tradingData.AddSignalAndItsOrderAndTrade(openSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick { Symbol = "RTS-9.13_FT", Price = 151000 });

            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void do_nothing_on_break_to_high_when_short_position_exists_for_strategy_test()
        {
            Signal openSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Market, 0, 0, 0);
            this.tradingData.AddSignalAndItsOrderAndTrade(openSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick { Symbol = "RTS-9.13_FT", Price = 151000 });

            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void do_nothing_on_break_to_low_when_long_position_exists_for_strategy_test()
        {
            Signal openSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 0, 0, 0);
            this.tradingData.AddSignalAndItsOrderAndTrade(openSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick { Symbol = "RTS-9.13_FT", Price = -1 });

            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void do_nothing_on_break_to_low_when_short_position_exists_for_strategy_test()
        {
            Signal openSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Market, 0, 0, 0);
            this.tradingData.AddSignalAndItsOrderAndTrade(openSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick { Symbol = "RTS-9.13_FT", Price = -1 });

            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void make_signal_to_buy_when_price_breaks_to_high_test()
        {
            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick { Symbol = "RTS-9.13_FT", Price = 20 });

            Assert.AreEqual(1, this.signalQueue.Count);

            Signal signal = this.signalQueue.Dequeue();

            Assert.AreEqual(0, signal.Stop);
            Assert.AreEqual(0, signal.Limit);
            Assert.AreEqual(18, signal.Price);
            Assert.AreEqual(OrderType.Market, signal.OrderType);
            Assert.AreEqual(TradeAction.Buy, signal.TradeAction);
            Assert.AreEqual(this.strategy, signal.Strategy);
            Assert.AreEqual(this.strategy.Id, signal.StrategyId);
            Assert.IsTrue(signal.Id > 0);
        }

        [TestMethod]
        public void make_signal_to_sell_when_price_breaks_to_low_test()
        {
            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick { Symbol = "RTS-9.13_FT", Price = -1 });

            Assert.AreEqual(1, this.signalQueue.Count);

            Signal signal = this.signalQueue.Dequeue();

            Assert.AreEqual(0, signal.Stop);
            Assert.AreEqual(0, signal.Limit);
            Assert.AreEqual(0, signal.Price);
            Assert.AreEqual(OrderType.Market, signal.OrderType);
            Assert.AreEqual(TradeAction.Sell, signal.TradeAction);
            Assert.AreEqual(this.strategy, signal.Strategy);
            Assert.AreEqual(this.strategy.Id, signal.StrategyId);
            Assert.IsTrue(signal.Id > 0);
        }

        [TestMethod]
        public void ignore_break_to_high_when_unfilled_market_buy_order_exists()
        {
            Signal openSignal =
                new Signal(this.strategy,
                    BrokerDateTime.Make(DateTime.Now),
                    TradeAction.Buy,
                    OrderType.Market,
                    0,
                    0,
                    0);
            this.tradingData.AddSignalAndItsOrder(openSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, 20, 1));
            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void ignore_break_to_low_when_unfilled_market_buy_order_exists()
        {
            Signal openSignal =
                new Signal(this.strategy,
                    BrokerDateTime.Make(DateTime.Now),
                    TradeAction.Buy,
                    OrderType.Market,
                    0,
                    0,
                    0);
            this.tradingData.AddSignalAndItsOrder(openSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, -1, 1));
            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void ignore_break_to_low_when_unfilled_market_sell_order_exists()
        {
            Signal openSignal =
                new Signal(this.strategy,
                    BrokerDateTime.Make(DateTime.Now),
                    TradeAction.Sell,
                    OrderType.Market,
                    0,
                    0,
                    0);
            this.tradingData.AddSignalAndItsOrder(openSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, -1, 1));
            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void ignore_break_to_high_when_unfilled_market_sell_order_exists()
        {
            Signal openSignal =
                new Signal(this.strategy,
                    BrokerDateTime.Make(DateTime.Now),
                    TradeAction.Sell,
                    OrderType.Market,
                    0,
                    0,
                    0);
            this.tradingData.AddSignalAndItsOrder(openSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, 20, 1));
            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void do_nothing_when_there_is_no_any_bars_in_trading_context_test()
        {
            this.tradingData.Get<ICollection<Bar>>().Clear();

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, 20, 1));
            Assert.AreEqual(0, this.signalQueue.Count);
        }

        [TestMethod]
        public void do_nothing_when_there_is_no_enough_bars_in_trading_context_test()
        {
            Bar last = this.tradingData.Get<ICollection<Bar>>().Last();

            this.tradingData.Get<ICollection<Bar>>().Remove(last);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, 20, 1));
            Assert.AreEqual(0, this.signalQueue.Count);
        }
    }
}
