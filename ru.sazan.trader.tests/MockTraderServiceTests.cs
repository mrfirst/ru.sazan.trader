﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.tests.Mocks;

namespace ru.sazan.trader.tests
{
    [TestClass]
    public class MockTraderServiceTests
    {
        private Service service;

        [TestInitialize]
        public void Setup()
        {
            this.service = new MockTraderService();
        }

        [TestMethod]
        public void Service_Start_Stop()
        {
            this.service.Start();

            Assert.IsTrue(this.service.IsRunning);

            this.service.Stop();

            Assert.IsFalse(this.service.IsRunning);
        }

        [TestMethod]
        public void Service_Restart()
        {
            Assert.IsFalse(this.service.IsRunning);

            this.service.Restart();

            Assert.IsTrue(this.service.IsRunning);
        }

        [TestMethod]
        public void Service_Restart_Running()
        {
            this.service.Start();

            Assert.IsTrue(this.service.IsRunning);

            this.service.Restart();

            Assert.IsTrue(this.service.IsRunning);
        }

    }
}
