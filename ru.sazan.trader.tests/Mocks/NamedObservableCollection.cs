﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Mocks
{
    public class NamedObservableCollection<T> : ObservableCollection<T>, Named
    {
        private string name;
        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public NamedObservableCollection(string name)
        {
            this.name = name;
        }
    }
}
