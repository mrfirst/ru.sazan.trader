﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Emulation;

namespace ru.sazan.trader.tests.Mocks
{
    [TestClass]
    public class SampleTradingDataContextFactoryTests
    {
        private DataContext tdContext;

        [TestInitialize]
        public void Setup()
        {
            this.tdContext = SampleTradingDataContextFactory.Make();
        }

        [TestMethod]
        public void TradingContext_contains_three_strategies_test()
        {
            Assert.AreEqual(5, this.tdContext.Get<IEnumerable<Strategy>>().Count());
            Assert.IsTrue(this.tdContext.Get<IEnumerable<Strategy>>().Any(s => s.Symbol.Equals("RTS-12.13_FT")));
            Assert.IsTrue(this.tdContext.Get<IEnumerable<Strategy>>().Any(s => s.Symbol.Equals("Si-12.13_FT")));
            Assert.IsTrue(this.tdContext.Get<IEnumerable<Strategy>>().Any(s => s.Symbol.Equals("Eu-12.13_FT")));
            Assert.IsTrue(this.tdContext.Get<IEnumerable<Strategy>>().Any(s => s.Symbol.Equals("SBRF-12.13_FT")));
            Assert.IsTrue(this.tdContext.Get<IEnumerable<Strategy>>().Any(s => s.Symbol.Equals("SBPR-12.13_FT")));
        }

        [TestMethod]
        public void TradingContext_contains_SymbolSettings_for_strategies_test()
        {
            Assert.AreEqual(5, this.tdContext.Get<IEnumerable<Symbol>>().Count());

            foreach (Strategy strategy in this.tdContext.Get<IEnumerable<Strategy>>())
                Assert.IsTrue(this.tdContext.Get<IEnumerable<Symbol>>().Any(s => s.Name.Equals(strategy.Symbol)));
        }
    }
}
