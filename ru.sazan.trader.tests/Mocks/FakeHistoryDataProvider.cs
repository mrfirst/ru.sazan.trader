﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Mocks
{
    public class FakeHistoryDataProvider:HistoryDataProvider
    {
        private DataContext tradingData;

        public FakeHistoryDataProvider(DataContext tradingData)
        {
            this.tradingData = tradingData;
        }

        public void Send(HistoryDataRequest request)
        {
            for (int i = 0; i < request.Quantity; i++)
                this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(request.Symbol, request.FromDate.AddSeconds(-1), TradeAction.Buy, 1, 1));
        }
    }
}
