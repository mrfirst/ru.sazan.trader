﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ru.sazan.trader.tests
{
    [TestClass]
    public class DefaultLoggerTests
    {
        [TestMethod]
        public void DefaultLogger_Instance_Is_Singleton()
        {
            Logger l = DefaultLogger.Instance;
            Logger l1 = DefaultLogger.Instance;

            Assert.AreSame(l, l1);
        }
    }
}
