﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.tests.Handlers
{
    [TestClass]
    public class CancelStopOrderOnTradeTests
    {
        private DataContext tradingData;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            UpdatePositionOnTrade handler = new UpdatePositionOnTrade(this.tradingData, new NullLogger());
            CancelStopOrderOnTrade cancelHandler = new CancelStopOrderOnTrade(this.tradingData, new NullLogger());
        }

        [TestMethod]
        public void cancel_long_position_stop_order_when_take_profit_order_begin_filling()
        {
            // Добавляем новую стратегию
            Strategy strategy = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 15);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            // Добавляем настройки stop loss для стратегии
            StopPointsSettings slSettings = new StopPointsSettings(strategy, 900, false);
            this.tradingData.Get<ICollection<StopPointsSettings>>().Add(slSettings);

            // Добавляем настройки take profit для стратегии
            ProfitPointsSettings tpSettings = new ProfitPointsSettings(strategy, 1000, false);
            this.tradingData.Get<ICollection<ProfitPointsSettings>>().Add(tpSettings);

            // Имитируем генерацию сигнала на открытие позиции
            Signal signal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Limit, 150000, 0, 150000);
            this.tradingData.Get<ICollection<Signal>>().Add(signal);

            Order order = new Order(signal);
            this.tradingData.Get<ICollection<Order>>().Add(order);

            // Брокер исполнил заявку на открытие позиции одной сделкой
            Trade trade = new Trade(order, order.Portfolio, order.Symbol, 150000, order.Amount, BrokerDateTime.Make(DateTime.Now));
            this.tradingData.Get<ObservableHashSet<Trade>>().Add(trade);

            // Позиция существует и ее объем равен объему в исполненной сделке
            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Position>>().Count());
            Assert.AreEqual(15, this.tradingData.GetAmount(strategy));

            // Имитируем автоматическую генерацию сигнала на установку stop loss заявки
            Signal slSignal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Stop, 150000, 149100, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(slSignal);

            Order slOrder = new Order(slSignal);
            this.tradingData.Get<ICollection<Order>>().Add(slOrder);

            // Имитируем автоматическую генерацию сигнала на установку takep profit заявки
            Signal tpSignal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Limit, 150000, 0, 151000);
            this.tradingData.Get<ICollection<Signal>>().Add(tpSignal);

            Order tpOrder = new Order(tpSignal);
            this.tradingData.Get<ICollection<Order>>().Add(tpOrder);

            // Рынок дошел до take profit но заявка исполнилась лишь частично
            Trade tpTrade = new Trade(tpOrder, tpOrder.Portfolio, tpOrder.Symbol, 151000, -8, BrokerDateTime.Make(DateTime.Now));
            this.tradingData.Get<ObservableHashSet<Trade>>().Add(tpTrade);

            // Объем позиции обновился
            Assert.AreEqual(7, this.tradingData.GetAmount(strategy));

            // Обработчик автоматически отправил запрос на отмену stop loss заявки как только начала исполняться лимитная заявка
            Assert.AreEqual(1, this.tradingData.Get<ObservableHashSet<OrderCancellationRequest>>().Count);
            OrderCancellationRequest request = this.tradingData.Get<ObservableHashSet<OrderCancellationRequest>>().Last();
            Assert.AreEqual(slOrder, request.Order);
        }

        [TestMethod]
        public void cancel_short_position_stop_order_when_take_profit_order_begin_filling()
        {
            // Добавляем новую стратегию
            Strategy strategy = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 15);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            // Добавляем настройки stop loss для стратегии
            StopPointsSettings slSettings = new StopPointsSettings(strategy, 900, false);
            this.tradingData.Get<ICollection<StopPointsSettings>>().Add(slSettings);

            // Добавляем настройки take profit для стратегии
            ProfitPointsSettings tpSettings = new ProfitPointsSettings(strategy, 1000, false);
            this.tradingData.Get<ICollection<ProfitPointsSettings>>().Add(tpSettings);

            // Имитируем генерацию сигнала на открытие позиции
            Signal signal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Limit, 150000, 0, 150000);
            this.tradingData.Get<ICollection<Signal>>().Add(signal);

            Order order = new Order(signal);
            this.tradingData.Get<ICollection<Order>>().Add(order);

            // Брокер исполнил заявку на открытие позиции одной сделкой
            Trade trade = new Trade(order, order.Portfolio, order.Symbol, 150000, -order.Amount, BrokerDateTime.Make(DateTime.Now));
            this.tradingData.Get<ObservableHashSet<Trade>>().Add(trade);

            // Позиция существует и ее объем равен объему в исполненной сделке
            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Position>>().Count());
            Assert.AreEqual(-15, this.tradingData.GetAmount(strategy));

            // Имитируем автоматическую генерацию сигнала на установку stop loss заявки
            Signal slSignal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Stop, 150000, 150900, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(slSignal);

            Order slOrder = new Order(slSignal);
            this.tradingData.Get<ICollection<Order>>().Add(slOrder);

            // Имитируем автоматическую генерацию сигнала на установку takep profit заявки
            Signal tpSignal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Limit, 150000, 0, 149000);
            this.tradingData.Get<ICollection<Signal>>().Add(tpSignal);

            Order tpOrder = new Order(tpSignal);
            this.tradingData.Get<ICollection<Order>>().Add(tpOrder);

            // Рынок дошел до take profit но заявка исполнилась лишь частично
            Trade tpTrade = new Trade(tpOrder, tpOrder.Portfolio, tpOrder.Symbol, 149000, 8, BrokerDateTime.Make(DateTime.Now));
            this.tradingData.Get<ObservableHashSet<Trade>>().Add(tpTrade);

            // Объем позиции обновился
            Assert.AreEqual(-7, this.tradingData.GetAmount(strategy));

            // Обработчик автоматически отправил запрос на отмену stop loss заявки как только начала исполняться лимитная заявка
            Assert.AreEqual(1, this.tradingData.Get<ObservableHashSet<OrderCancellationRequest>>().Count);
            OrderCancellationRequest request = this.tradingData.Get<ObservableHashSet<OrderCancellationRequest>>().Last();
            Assert.AreEqual(slOrder, request.Order);
        }
    }
}
