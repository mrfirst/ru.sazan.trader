﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Events;
using ru.sazan.trader.Handlers;

namespace ru.sazan.trader.tests.Handlers
{
    public class SampleAddedItemHandler : AddedItemHandler<double>
    {
        public SampleAddedItemHandler(ItemHasBeenAddedNotifier<double> notifier)
            :base(notifier){}

        private double inner;
        public double Inner
        {
            get
            {
                return this.inner;
            }
        }

        public override void OnItemAdded(double item)
        {
            this.inner = item;
        }
    }

    [TestClass]
    public class AddedItemHandlerTests
    {
        private ObservableCollection<double> collection;
        private SampleAddedItemHandler handler;

        [TestInitialize]
        public void Setup()
        {
            this.collection = new ObservableCollection<double>();
            this.handler = new SampleAddedItemHandler(this.collection);
        }

        [TestMethod]
        public void AddedItemHander_Execute_OnItemAdded_test()
        {
            Assert.AreEqual(0, this.handler.Inner);

            this.collection.Add(28);

            Assert.AreEqual(28, this.handler.Inner);
        }
    }
}
