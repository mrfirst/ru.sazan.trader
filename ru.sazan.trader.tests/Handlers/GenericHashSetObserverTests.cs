﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.tests.Mocks;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.tests.Handlers
{
    public class MockGenericHashSetObserver : GenericHashSetObserver<Trade>
    {
        public MockGenericHashSetObserver(ObservableHashSetFactory tradingData)
            : base(tradingData)
        {
        }

        public int ShotCounter { get; set; }

        public override void Update(Trade item)
        {
            this.ShotCounter++;
        }
    }

    [TestClass]
    public class GenericHashSetObserverTests
    {
        private DataContext tradingData;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();

            
        }

        [TestMethod]
        public void MockObserver_Test()
        {

            GenericHashSetObserver<Trade> trigger = new MockGenericHashSetObserver((ObservableHashSetFactory)this.tradingData);
            MockGenericHashSetObserver mockTrigger = (MockGenericHashSetObserver)trigger;

            Strategy s = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            Signal signal = new Signal(s, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 130000, 0, 0);
            Order order = new Order(signal);

            Assert.AreEqual(0, mockTrigger.ShotCounter);

            this.tradingData.Get<ObservableHashSet<Trade>>().Add(new Trade(order, s.Portfolio, s.Symbol, 131000, 4, BrokerDateTime.Make(DateTime.Now)));

            Assert.AreEqual(1, mockTrigger.ShotCounter);
        }
    }
}
