﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.tests.Mocks;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Emulation;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Handlers;

namespace ru.sazan.trader.tests.Handlers
{
    [TestClass]
    public class TrailStopOnTickTests
    {
        private DataContext tradingData;
        private Strategy strategy;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();

            this.strategy = new Strategy(1, "Description", "Portfolio", "Symbol", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(this.strategy);

            TrailStopOnTick handler =
                new TrailStopOnTick(this.strategy,
                    this.tradingData,
                    new NullLogger());

            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<MoveOrder>>().Count());
        }

        
        [TestMethod]
        public void trail_long_position_stop_order_when_tick_price_is_greater_than_stop_test()
        {
            Signal oSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.tradingData.AddSignalAndItsOrderAndTrade(oSignal);

            Signal sSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Stop, 150010, 149910, 0);
            this.tradingData.AddSignalAndItsOrder(sSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, DateTime.Now, TradeAction.Sell, 150020, 10));

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<MoveOrder>>().Count());
        }

        [TestMethod]
        public void do_not_trail_long_position_stop_when_tick_price_is_lower_than_stop_test()
        {
            Signal oSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.tradingData.AddSignalAndItsOrderAndTrade(oSignal);

            Signal sSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Stop, 150010, 149910, 0);
            this.tradingData.AddSignalAndItsOrder(sSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, DateTime.Now, TradeAction.Sell, 150020, 10));

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<MoveOrder>>().Count());
        }

        [TestMethod]
        public void trail_short_position_stop_order_when_tick_price_move_down_test()
        {
            Signal oSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Market, 150000, 0, 0);
            this.tradingData.AddSignalAndItsOrderAndTrade(oSignal);

            Signal sSignal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Stop, 150010, 150110, 0);
            this.tradingData.AddSignalAndItsOrder(sSignal);

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick(this.strategy.Symbol, DateTime.Now, TradeAction.Sell, 150000, 10));

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<MoveOrder>>().Count());
        }

    }
}
