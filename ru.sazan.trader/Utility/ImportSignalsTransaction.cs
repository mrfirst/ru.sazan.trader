﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using System.IO;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Utility
{
    public class ImportSignalsTransaction:Transaction
    {
        private DataContext tradingData;
        private string importFileName;

        private ImportSignalsTransaction() { }

        public ImportSignalsTransaction(DataContext tradingData, string importFileName)
        {
            this.tradingData = tradingData;
            this.importFileName = importFileName;
        }

        public void Execute()
        {
            if (!File.Exists(this.importFileName))
                return;

            StreamReader streamReader = new StreamReader(this.importFileName);
            StringReader stringReader = new StringReader(streamReader.ReadToEnd());

            while (true)
            {
                string line = stringReader.ReadLine();

                if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line))
                {
                    try
                    {
                        Signal signal = Signal.Parse(line);

                        Strategy strategy = GetStrategy(signal.StrategyId);

                        if (strategy != null)
                        {
                            signal.Strategy = strategy;
                            this.tradingData.Get<ICollection<Signal>>().Add(signal);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                    break;
            }

            stringReader.Close();
            stringReader.Dispose();
            streamReader.Close();
            streamReader.Dispose();
        }

        private Strategy GetStrategy(int id)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Strategy>>().Single(s => s.Id == id);
            }
            catch
            {
                return null;
            }
        }

    }
}
