﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Utility
{
    public interface TradingSchedule
    {
        bool ItIsTimeToTrade(DateTime dateTime);
        DateTime SessionEnd { get; }
    }
}
