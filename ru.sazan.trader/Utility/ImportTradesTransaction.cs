﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using System.IO;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Utility
{
    public class ImportTradesTransaction:Transaction
    {
        private ObservableHashSetFactory tradingData;
        private string path;

        private ImportTradesTransaction() { }

        public ImportTradesTransaction(ObservableHashSetFactory tradingData, string path)
        {
            this.tradingData = tradingData;
            this.path = path;
        }

        public void Execute()
        {
            if (!File.Exists(this.path))
                return;

            StreamReader streamReader = new StreamReader(this.path);
            StringReader stringReader = new StringReader(streamReader.ReadToEnd());

            while (true)
            {
                string line = stringReader.ReadLine();

                if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line))
                {
                    try
                    {
                        Trade trade = Trade.Parse(line);

                        Order order = GetOrder(trade.OrderId);

                        if (order != null)
                        {
                            trade.Order = order;
                            this.tradingData.Make<Trade>().Add(trade);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                    break;
            }

            stringReader.Close();
            stringReader.Dispose();
            streamReader.Close();
            streamReader.Dispose();
        }

        private Order GetOrder(int id)
        {
            try
            {
                return this.tradingData.Make<Order>().Single(s => s.Id == id);
            }
            catch
            {
                return null;
            }
        }

    }
}
