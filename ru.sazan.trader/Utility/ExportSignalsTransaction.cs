﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using System.IO;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Utility
{
    public class ExportSignalsTransaction:Transaction
    {
        private ObservableHashSetFactory tradingData;
        private string path;

        public ExportSignalsTransaction(ObservableHashSetFactory tradingData, string path)
        {
            this.tradingData = tradingData;
            this.path = path;
        }

        public void Execute()
        {
            if (this.tradingData.Make<Signal>().Count == 0)
            {
                if (File.Exists(this.path))
                    File.Delete(this.path);

                return;
            }

            FileStream fileStream = new FileStream(this.path, FileMode.Create, FileAccess.ReadWrite);
            StreamWriter streamWriter = new StreamWriter(fileStream, Encoding.UTF8);

            foreach (Signal item in this.tradingData.Make<Signal>())
            {
                streamWriter.WriteLine(item.ToImportString());
                streamWriter.Flush();
            }

            streamWriter.Close();
            streamWriter.Dispose();
            fileStream.Close();
            fileStream.Dispose();
        }

    }
}
