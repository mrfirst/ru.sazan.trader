﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using System.IO;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.Utility
{
    public class ImportBarsTransaction : ImportItemsTransaction<Bar>
    {
        public ImportBarsTransaction(ObservableCollection<Bar> dst, string path)
            :base(dst, path){}

        public override Bar TryParseItem(string src)
        {
            try
            {
                return Bar.Parse(src);
            }
            catch
            {
                return null;
            }
        }
    }

}
