﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Utility
{
    public class BrokerDateTime
    {
        public static DateTime Make(DateTime localDate)
        {
            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(ReadBrokerServerTimezoneIdFromAppConfig());

            return TimeZoneInfo.ConvertTime(localDate, timeZoneInfo);
        }

        private static string ReadBrokerServerTimezoneIdFromAppConfig()
        {
            return AppSettings.GetStringValue("BrokerServerTimezone");
        }

        public static DateTime TodayMidnight()
        {
            DateTime tomorrow = BrokerDateTime.Make(DateTime.Now).AddDays(1);

            return new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 0, 0, 0, 0);
        }

    }
}
