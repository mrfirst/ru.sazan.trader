﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Models;
using ru.sazan.trader.Indicators;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Inputs
{
    public class BuyOnTick:AddedItemHandler<Tick>
    {
        private Strategy strategy;
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private Logger logger;

        public BuyOnTick(Strategy strategy, DataContext tradingData, ObservableQueue<Signal> signalQueue, Logger logger)
            :base(tradingData.Get<ObservableCollection<Tick>>())
        {
            this.strategy = strategy;
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;
        }

        public override void OnItemAdded(Tick item)
        {
            if (item.Symbol != this.strategy.Symbol)
                return;

            BarSettings bs = this.tradingData.Get<IEnumerable<BarSettings>>().SingleOrDefault(s => s.StrategyId == this.strategy.Id);

            if (bs == null)
                return;

            SMASettings ss = this.tradingData.Get<IEnumerable<SMASettings>>().SingleOrDefault(s => s.StrategyId == this.strategy.Id);

            if (ss == null)
                return;

            double strategyAmount = this.tradingData.GetAmount(this.strategy);
            
            if (strategyAmount > 0)
                return;

            if (this.tradingData.UnfilledExists(this.strategy, OrderType.Limit))
                return;

            IEnumerable<Bar> bars = this.tradingData.Get<IEnumerable<Bar>>().GetNewestBars(bs.Symbol, bs.Interval, bs.Period + 1);

            if (bars == null || bars.Count() == 0)
                return;

            if (bars.Count() < ss.FastPeriod + 1)
                return;

            if (bars.Count() < ss.SlowPeriod + 1)
                return;

            IEnumerable<double> closePrices = from b in bars
                                              select b.Close;

            IEnumerable<double> fastSMA = Ema.Make(closePrices, ss.FastPeriod);
            IEnumerable<double> slowSMA = Ema.Make(closePrices, ss.SlowPeriod);
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, fast {2} slow {3}", DateTime.Now, this.GetType().Name, fastSMA.Last(), slowSMA.Last()));

            if (slowSMA.Last() < fastSMA.Last())
            {
                Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Limit, item.Price, 0, item.Price);
                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал на открытие длинной позиции {2}", DateTime.Now, this.GetType().Name, signal.ToString()));
                this.signalQueue.Enqueue(signal);
            }
            else if (slowSMA.Last() > fastSMA.Last())
            {
                Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Limit, item.Price, 0, item.Price);
                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал на открытие короткой позиции {2}", DateTime.Now, this.GetType().Name, signal.ToString()));
                this.signalQueue.Enqueue(signal);
            }

        }

    }
}
