﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Inputs
{
    public class BreakOutOnTick:AddedItemHandler<Tick>
    {
        private Strategy strategy;
        private BarSettings barSettings;
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private Logger logger;

        private IEnumerable<Bar> bars;
        private double high, low;
        private Signal signal;

        public BreakOutOnTick(Strategy strategy, DataContext tradingData, ObservableQueue<Signal> signalQueue, Logger logger)
            :base(tradingData.Get<ObservableCollection<Tick>>())
        {
            this.strategy = strategy;
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;

            this.barSettings = this.tradingData.Get<IEnumerable<BarSettings>>().SingleOrDefault(s => s.StrategyId == this.strategy.Id);
        }

        public override void OnItemAdded(Tick item)
        {
            ClearTemporaryProperties();

            if (this.barSettings == null)
                return;

            if (this.strategy.Symbol != item.Symbol)
                return;

            if (this.tradingData.PositionExists(this.strategy))
                return;

            if (this.tradingData.UnfilledExists(this.strategy, OrderType.Market))
                return;

            UpdateBars();

            if (this.bars == null)
                return;

            if (this.bars.Count() < this.barSettings.Period)
                return;

            UpdateHigh();
            UpdateLow();

            if (this.high < item.Price)
                this.signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, this.high, 0, 0);

            if (this.low > item.Price)
                this.signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Market, this.low, 0, 0);

            if (this.signal == null)
                return;

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал пробой {2}", DateTime.Now, this.GetType().Name, signal.ToString()));

            this.signalQueue.Enqueue(signal);
        }

        private void ClearTemporaryProperties()
        {
            this.signal = null;
            this.bars = null;
            this.high = 0;
            this.low = 0;
        }

        private void UpdateBars()
        {
            this.bars = this.tradingData.Get<IEnumerable<Bar>>().GetNewestBars(this.barSettings.Symbol, this.barSettings.Interval, this.barSettings.Period);
        }

        private void UpdateHigh()
        {
            this.high = this.bars.Max(b => b.High);
        }

        private void UpdateLow()
        {
            this.low = this.bars.Min(b => b.Low);
        }
    }
}
