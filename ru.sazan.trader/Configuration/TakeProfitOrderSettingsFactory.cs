﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Configuration
{
    public class TakeProfitOrderSettingsFactory:GenericFactory<TakeProfitOrderSettings>
    {
        private Strategy strategy;
        private string prefix;

        public TakeProfitOrderSettingsFactory(Strategy strategy, string prefix)
        {
            this.strategy = strategy;
            this.prefix = prefix;
        }

        public TakeProfitOrderSettings Make()
        {
            try
            {
                return new TakeProfitOrderSettings(this.strategy,
                    AppSettings.GetValue<int>(String.Concat(this.prefix, "_TakeProfitOrderSettings_TimeToLive")));
            }
            catch
            {
                return null;
            }
        }
    }
}
