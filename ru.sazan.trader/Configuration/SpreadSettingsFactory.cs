﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Configuration
{
    public class SpreadSettingsFactory:GenericFactory<SpreadSettings>
    {
        private string prefix;

        public SpreadSettingsFactory(string prefix)
        {
            this.prefix = prefix;
        }

        public SpreadSettings Make()
        {
            try
            {
                return new SpreadSettings(AppSettings.GetValue<double>(String.Concat(this.prefix, "_FairPrice")),
                    AppSettings.GetValue<double>(String.Concat(this.prefix, "_SellAfter")),
                    AppSettings.GetValue<double>(String.Concat(this.prefix, "_BuyBefore")));
            }
            catch
            {
                return null;
            }
        }
    }
}
