﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Configuration
{
    public class StopLossOrderSettingsFactory:GenericFactory<StopLossOrderSettings>
    {
        private Strategy strategy;
        private string prefix;

        public StopLossOrderSettingsFactory(Strategy strategy, string prefix)
        {
            this.strategy = strategy;
            this.prefix = prefix;
        }

        public StopLossOrderSettings Make()
        {
            try
            {
                return new StopLossOrderSettings(this.strategy,
                    AppSettings.GetValue<int>(String.Concat(this.prefix, "_StopLossOrderSettings_TimeToLive")));
            }
            catch
            {
                return null;
            }
        }
    }
}
