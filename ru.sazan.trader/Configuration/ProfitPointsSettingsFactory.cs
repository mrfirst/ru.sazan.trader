﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Configuration
{
    public class ProfitPointsSettingsFactory:GenericFactory<ProfitPointsSettings>
    {
        private Strategy strategy;
        private string prefix;

        private ProfitPointsSettingsFactory() { }

        public ProfitPointsSettingsFactory(Strategy strategy, string prefix)
        {
            this.strategy = strategy;
            this.prefix = prefix;
        }

        public ProfitPointsSettings Make()
        {
            try
            {
                return new ProfitPointsSettings(this.strategy,
                    AppSettings.GetValue<double>(String.Concat(this.prefix, "_ProfitPointsSettings_Points")),
                    AppSettings.GetValue<bool>(String.Concat(this.prefix, "_ProfitPointsSettings_Trail")));
            }
            catch
            {
                return null;
            }
        }
    }
}
