﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Configuration
{
    public class StopPointsSettingsFactory:GenericFactory<StopPointsSettings>
    {
        private Strategy strategy;
        private string prefix;

        private StopPointsSettingsFactory() { }

        public StopPointsSettingsFactory(Strategy strategy, string prefix)
        {
            this.strategy = strategy;
            this.prefix = prefix;
        }

        public StopPointsSettings Make()
        {
            try
            {
                return new StopPointsSettings(this.strategy,
                    AppSettings.GetValue<double>(String.Concat(this.prefix, "_StopPointsSettings_Points")),
                    AppSettings.GetValue<bool>(String.Concat(this.prefix, "_StopPointsSettings_Trail")));
            }
            catch
            {
                return null;
            }
        }
    }
}
