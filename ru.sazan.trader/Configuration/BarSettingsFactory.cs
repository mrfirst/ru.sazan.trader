﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Configuration
{
    public class BarSettingsFactory:GenericFactory<BarSettings>
    {
        private Strategy strategy;
        private string prefix;

        private BarSettingsFactory() { }

        public BarSettingsFactory(Strategy strategy, string prefix)
        {
            this.strategy = strategy;
            this.prefix = prefix;
        }

        public BarSettings Make()
        {
            try
            {
                return new BarSettings(this.strategy,
                    AppSettings.GetStringValue(String.Concat(this.prefix, "_BarSettings_Symbol")),
                    AppSettings.GetValue<int>(String.Concat(this.prefix, "_BarSettings_Interval")),
                    AppSettings.GetValue<int>(String.Concat(this.prefix, "_BarSettings_Period")));
            }
            catch
            {
                return null;
            }
        }
    }
}
