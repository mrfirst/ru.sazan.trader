﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public class PointsSettings:Identified
    {
        public int Id { get; set; }
        public Strategy Strategy { get; set; }
        public int StrategyId { get; set; }
        public double Points { get; set; }
        public bool Trail { get; set; }

        protected PointsSettings() { }

        public PointsSettings(Strategy strategy, double points, bool trail)
        {
            this.Id = strategy.Id;
            this.Strategy = strategy;
            this.StrategyId = strategy.Id;
            this.Points = points;
            this.Trail = trail;
        }
    }
}
