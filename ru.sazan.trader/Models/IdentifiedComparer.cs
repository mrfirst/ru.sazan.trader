﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public class IdentifiedComparer : EqualityComparer<Identified>
    {
        public override bool Equals(Identified x, Identified y)
        {
            if (x.Id == y.Id)
                return true;

            return false;
        }

        public override int GetHashCode(Identified obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
