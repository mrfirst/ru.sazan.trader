﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public enum PositionType
    {
        Any = 0,
        Long = 1,
        Short = 2
    }

    public class PositionSettings:Identified
    {
        public int Id { get; set; }
        public Strategy Strategy { get; set; }
        public int StrategyId { get; set; }
        public PositionType PositionType { get; set; }

        public PositionSettings() { }

        public PositionSettings(Strategy strategy)
            :this(strategy, PositionType.Any)
        {
        }

        public PositionSettings(Strategy strategy, PositionType type)
        {
            this.Id = strategy.Id;
            this.Strategy = strategy;
            this.StrategyId = strategy.Id;
            this.PositionType = type;
        }
    }
}
