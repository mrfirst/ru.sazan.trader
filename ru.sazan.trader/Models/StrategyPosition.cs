﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public class StrategyPosition:Identified
    {
        public int Id { get; set; }
        
        public Strategy Strategy { get; set; }
        
        public int StrategyId { get; set; }
        
        private List<Trade> trades;

        public IEnumerable<Trade> Trades
        {
            get
            {
                return this.trades;
            }
        }

        private StrategyPosition() 
        {
            this.trades = new List<Trade>();
        }

        public StrategyPosition(Strategy strategy)
            :this()
        {
            this.Id = strategy.Id;
            this.StrategyId = strategy.Id;
            this.Strategy = strategy;
            this.amount = 0;
            this.sum = 0;
        }

        private double amount;
        public double Amount
        {
            get
            {
                return this.amount;
            }
        }

        private double sum;
        public double Sum
        {
            get
            {
                return this.sum;
            }
        }
    }
}
