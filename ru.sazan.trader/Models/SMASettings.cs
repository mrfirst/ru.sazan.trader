﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Models
{
    public class SMASettings:Identified
    {
        public int Id { get; set; }
        public Strategy Strategy { get; set; }
        public int StrategyId { get; set; }
        public int FastPeriod { get; set; }
        public int SlowPeriod { get; set; }

        public SMASettings(Strategy strategy, int fastPeriod, int slowPeriod)
        {
            this.Id = SerialIntegerFactory.Make();
            this.Strategy = strategy;
            this.StrategyId = strategy.Id;
            this.FastPeriod = fastPeriod;
            this.SlowPeriod = slowPeriod;
        }
    }
}
