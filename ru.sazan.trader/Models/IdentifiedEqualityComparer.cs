﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public class IdentifiedEqualityComparer<T>:IEqualityComparer<T>
        where T : Identified
    {
        public bool Equals(T left, T right)
        {
            if (left.Id == right.Id)
                return true;

            return false;
        }

        public int GetHashCode(T item)
        {
            return item.Id.GetHashCode();
        }
    }
}
