﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Models
{
    public class BarSettings:Identified
    {
        public int Id { get; set; }
        public string Symbol { get; set; }
        public int Interval { get; set; }
        public int Period { get; set; }
        public int StrategyId { get; set; }
        public virtual Strategy Strategy { get; set; }

        public BarSettings(Strategy strategy, string symbol, int interval, int period)
        {
            this.Id = SerialIntegerFactory.Make();
            this.Symbol = symbol;
            this.Interval = interval;
            this.Period = period;
            this.StrategyId = strategy.Id;
            this.Strategy = strategy;
        }
    }
}
