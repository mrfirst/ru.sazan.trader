﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public interface Mutable<T>
    {
        void Update(T item);
    }
}
