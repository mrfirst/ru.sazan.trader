﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Extensions
{
    public static class StrategyCollectionExtensions
    {
        public static double GetAmount(this ICollection<Strategy> collection, string portfolio, string symbol)
        {
            try
            {
                return collection.Where(s => s.Portfolio == portfolio
                    && s.Symbol == symbol).Sum(ps => ps.Amount);
            }
            catch
            {
                return 0;
            }
        }
    }
}
