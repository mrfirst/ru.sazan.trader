﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool Within(this DateTime dateTime, TimePeriod timePeriod)
        {
            return dateTime > timePeriod.Begin && dateTime < timePeriod.End;
        }

        public static bool ItIsWorkDay(this DateTime dateTime)
        {
            return dateTime.DayOfWeek != DayOfWeek.Sunday && dateTime.DayOfWeek != DayOfWeek.Saturday;
        }

        public static DateTime RoundDownToNearestMinutes(this DateTime date, int minutes)
        {
            int factor = date.Minute % minutes;

            int seconds = factor * 60 + date.Second;

            return date.AddSeconds(-seconds).AddMilliseconds(-date.Millisecond);
        }
    }
}
