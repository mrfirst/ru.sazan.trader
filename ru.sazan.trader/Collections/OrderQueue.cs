﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Collections
{
    public class OrderQueue:ObservableQueue<Order>
    {
        private static OrderQueue queue = null;

        public static OrderQueue Instance
        {
            get
            {
                if (queue == null)
                    queue = new OrderQueue();

                return queue;
            }
        }

        private OrderQueue() { }
    }
}
