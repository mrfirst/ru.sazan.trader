﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader
{
    public interface Service
    {
        void Start();
        void Stop();
        void Restart();
        bool IsRunning { get; }
    }
}
