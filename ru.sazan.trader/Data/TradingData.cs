﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Data
{
    public class TradingData:TradingDataContext
    {
        private static TradingData instance = null;

        public static TradingData Instance
        {
            get
            {
                if (instance == null)
                    instance = new TradingData();

                return instance;
            }
        }

        private TradingData() { }
    }
}
