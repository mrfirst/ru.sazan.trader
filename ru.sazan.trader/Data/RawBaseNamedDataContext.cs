﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Collections;
using System.Reflection;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Data
{
    public abstract class RawBaseNamedDataContext : NamedDataContext
    {
        public T Get<T>(string name) where T : Named
        {
            PropertyInfo[] properties = this.GetType().GetProperties();

            foreach (PropertyInfo item in properties)
            {
                object value = item.GetValue(this, null);

                if (value is T)
                {
                    T result = (T)value;

                    if(result.Name.Equals(name))
                        return result;
                }
            }

            return default(T);
        }
    }
}
