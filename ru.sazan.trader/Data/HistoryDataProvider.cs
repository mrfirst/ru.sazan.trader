﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Data
{
    public interface HistoryDataProvider
    {
        void Send(HistoryDataRequest request);
    }
}
