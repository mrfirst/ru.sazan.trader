﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Data
{
    public interface Database
    {
        void Add<T>(T item) where T : class;
        void Remove<T>(T item) where T : class;
    }
}
