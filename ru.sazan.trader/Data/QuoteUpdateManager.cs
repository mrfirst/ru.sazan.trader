﻿using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Data
{
    public interface QuoteUpdateManager
    {
        void Update(int index, string symbol, double bid, double bidVolume, double offer, double offerVolume);
    }
}
