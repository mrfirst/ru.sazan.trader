﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Data
{
    public interface Subscriber
    {
        void Subscribe();
        void Unsubscribe();
        int SubscriptionsCounter { get; }
    }
}
