﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Collections;
using System.Reflection;

namespace ru.sazan.trader.Data
{
    public abstract class BaseDataContext
    {
        public ObservableCollection<T> GetData<T>() where T : class
        {
            PropertyInfo[] properties = this.GetType().GetProperties();

            foreach (PropertyInfo item in properties)
            {
                object value = item.GetValue(this, null);

                if (value is ObservableCollection<T>)
                    return value as ObservableCollection<T>;
            }

            return null;
        }
    }
}
