﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Handlers
{
    public class CancelOrderOnTrade:AddedItemHandler<Trade>
    {
        private DataContext tradingData;
        private Logger logger;

        public CancelOrderOnTrade(DataContext tradingData, Logger logger)
            :base(tradingData.Get<ObservableHashSet<Trade>>())
        {
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void OnItemAdded(Trade item)
        {
            Strategy strategy = item.Order.Signal.Strategy;

            if (strategy == null)
                return;

            IEnumerable<Order> unfilled = this.tradingData.Get<ICollection<Order>>().GetUnfilled(strategy);

            if (unfilled == null || unfilled.Count() == 0)
                return;

            foreach (Order o in unfilled)
            {
                if (this.tradingData.GetAmount(strategy) == 0)
                {
                    if (!CancelOrderRequestExists(o.Id))
                    {

                        string descr = String.Format("Отменить заявку {0}, потому что позиция была закрыта заявкой {1}", o.ToString(), item.Order.ToString());
                        this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, {2}", DateTime.Now, this.GetType().Name, descr));
                        this.tradingData.Get<ObservableHashSet<OrderCancellationRequest>>().Add(new OrderCancellationRequest(o, descr));
                    }
                }
            }
        }

        private bool CancelOrderRequestExists(int orderId)
        {
            return this.tradingData.Get<IEnumerable<OrderCancellationRequest>>().Any(o => o.OrderId == orderId);
        }
    }
}
