﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;
using ru.sazan.trader.Events;

namespace ru.sazan.trader.Handlers
{
    public abstract class GenericCollectionObserver<T>:GenericObserver<T>
        where T:class
    {
        protected BaseDataContext dataContext;

        public GenericCollectionObserver(BaseDataContext dataContext)
        {
            this.dataContext = dataContext;
            this.dataContext.GetData<T>().RegisterObserver(this);
        }

        public abstract void Update(T item);
    }
}
