﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Handlers
{
    public abstract class QuotesHandler
    {
        protected SymbolDataHasBeenUpdatedNotifier quoteUpdateNotifier;

        public QuotesHandler(SymbolDataHasBeenUpdatedNotifier quoteUpdateNotifier)
        {
            this.quoteUpdateNotifier = quoteUpdateNotifier;
            this.quoteUpdateNotifier.OnQuotesUpdate += new SymbolDataHasBeenUpdatedNotification(OnQuotesUpdate);
        }

        public abstract void OnQuotesUpdate(string symbol);
    }
}
