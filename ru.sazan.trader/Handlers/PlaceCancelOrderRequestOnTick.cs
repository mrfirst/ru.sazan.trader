﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.Handlers
{
    public class PlaceCancelOrderRequestOnTick:AddedItemHandler<Tick>
    {
        private DataContext tradingData;
        private Logger logger;

        public PlaceCancelOrderRequestOnTick(DataContext tradingData, Logger logger)
            :base(tradingData.Get<ObservableCollection<Tick>>())
        {
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void OnItemAdded(Tick item)
        {
            IEnumerable<Order> unfilled = this.tradingData.Get<ICollection<Order>>().GetUnfilled(item.Symbol);

            if (unfilled == null)
                return;

            if (unfilled.Count() == 0)
                return;

            foreach (Order o in unfilled)
            {
                if (!this.tradingData.Get<ICollection<Position>>().Exists(o.Portfolio, o.Symbol))
                {
                    GenericFactory<OrderCancellationRequest> cancellationFactory = new UnfilledOrderCancellationRequestFactory(item.Price, o, (TradingDataContext)this.tradingData);

                    OrderCancellationRequest request = cancellationFactory.Make();

                    if (request != null)
                    {
                        this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сформирован новый запрос {2} на отмену заявки {3}.", DateTime.Now, this.GetType().Name, o.ToString(), request.Description));
                        this.tradingData.Get<ObservableHashSet<OrderCancellationRequest>>().Add(request);
                    }
                }
            }
        }
    }
}
