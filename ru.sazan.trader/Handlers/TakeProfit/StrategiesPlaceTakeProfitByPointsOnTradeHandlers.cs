﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Handlers.TakeProfit
{
    public class StrategiesPlaceTakeProfitByPointsOnTradeHandlers:HashSet<PlaceStrategyTakeProfitByPointsOnTrade>
    {
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private Logger logger;
        private bool measureStopFromSignal;

        public StrategiesPlaceTakeProfitByPointsOnTradeHandlers()
            : this(TradingData.Instance, SignalQueue.Instance, DefaultLogger.Instance) { }

        public StrategiesPlaceTakeProfitByPointsOnTradeHandlers(DataContext tradingData, 
            ObservableQueue<Signal> signalQueue,
            Logger logger,
            bool measureStopFromSignal = false)
        {
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;
            this.measureStopFromSignal = measureStopFromSignal;

            ActivateHandlers();
        }

        private void ActivateHandlers()
        {
            foreach (Strategy strategy in this.tradingData.Get<IEnumerable<Strategy>>())
            {
                if (!this.tradingData.Get<IEnumerable<ProfitPointsSettings>>().Any(s => s.Strategy.Id == strategy.Id))
                    continue;

                if (!this.tradingData.Get<IEnumerable<TakeProfitOrderSettings>>().Any(s => s.Strategy.Id == strategy.Id))
                    continue;

                base.Add(new PlaceStrategyTakeProfitByPointsOnTrade(strategy, this.tradingData, this.signalQueue, this.logger, this.measureStopFromSignal));
                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, PlaceStrategyTakeProfitByPointsOnTrade handler has been activated for {2} ", 
                    BrokerDateTime.Make(DateTime.Now), 
                    this.GetType().Name, 
                    strategy.ToString()));
            }
        }
    }
}
