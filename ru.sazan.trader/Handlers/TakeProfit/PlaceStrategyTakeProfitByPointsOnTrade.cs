﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers.TakeProfit
{
    public class PlaceStrategyTakeProfitByPointsOnTrade:StrategyTakeProfitOnItemAddedBase<Trade>
    {
        private double openPrice;

        public PlaceStrategyTakeProfitByPointsOnTrade(Strategy strategy)
            :this(strategy, 
            TradingData.Instance,
            SignalQueue.Instance,
            DefaultLogger.Instance) { }

        public PlaceStrategyTakeProfitByPointsOnTrade(Strategy strategy,
            DataContext tradingData,
            ObservableQueue<Signal> signalQueue,
            Logger logger,
            bool measureFromSignalPrice = false)
            :base(strategy, tradingData,signalQueue,logger,measureFromSignalPrice) { }

        private void UpdateOpenPrice(Trade item)
        {
            if (this.measureFromSignalPrice)
                this.openPrice = item.Order.Signal.Price;
            else
                this.openPrice = item.Price;
        }

        private void UpdateCloseSignalAmountWith(double amount)
        {
            this.signal.Amount = Math.Abs(amount);
        }

        private void MakeSignalToBuyWith(double price)
        {
            this.signal =
                new Signal(this.strategy,
                BrokerDateTime.Make(DateTime.Now),
                TradeAction.Buy,
                OrderType.Limit,
                price,
                0,
                this.openPrice - this.ppSettings.Points);
        }

        private void MakeSignalToSellWith(double price)
        {
            this.signal =
                new Signal(this.strategy,
                BrokerDateTime.Make(DateTime.Now),
                TradeAction.Sell,
                OrderType.Limit,
                price,
                0,
                this.openPrice + this.ppSettings.Points);
        }

        public override void MakeSignal(Trade item, double positionAmount)
        {

            UpdateOpenPrice(item);

            if (positionAmount > 0)
                MakeSignalToSellWith(item.Price);
            else if (positionAmount < 0)
                MakeSignalToBuyWith(item.Price);

            if (this.signal != null)
                UpdateCloseSignalAmountWith(positionAmount);
        }
    }
}
