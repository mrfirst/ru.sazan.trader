﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Handlers
{
    public class UpdateOrderAmountOnTrade:AddedItemHandler<Trade>
    {
        private Logger logger;

        public UpdateOrderAmountOnTrade(DataContext tradingData, Logger logger)
            :base(tradingData.Get<ObservableHashSet<Trade>>())
        {
            this.logger = logger;
        }

        public override void OnItemAdded(Trade item)
        {
            if (item.Order.IsFilled)
                return;

            if(item.Order.FilledAmount + item.AbsoluteAmount <= item.Order.Amount)
                item.Order.FilledAmount += item.AbsoluteAmount;

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, исполненный объем заявки обновлен {2}.", 
                BrokerDateTime.Make(DateTime.Now), this.GetType().Name, item.Order.ToString()));
        }
    }
}
