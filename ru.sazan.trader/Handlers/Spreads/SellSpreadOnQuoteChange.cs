﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data.Spreads;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.Handlers.Spreads
{
    public class SellSpreadOnQuoteChange
    {
        private IEnumerable<Strategy> leftLeg, rightLeg;
        private OrderBookContext quotesProvider;
        private DataContext tradingData;
        private Logger logger;
        private ObservableQueue<Signal> signalQueue;
        private SpreadSettings spreadSettings;

        public SellSpreadOnQuoteChange(OrderBookContext quotesProvider,
            IEnumerable<Strategy> leftLeg,
            IEnumerable<Strategy> rightLeg,
            SpreadSettings spreadSettings,
            DataContext dataContext,
            ObservableQueue<Signal> signalQueue,
            Logger logger)
        {
            this.quotesProvider = quotesProvider;
            this.leftLeg = leftLeg;
            this.rightLeg = rightLeg;
            this.spreadSettings = spreadSettings;
            this.tradingData = dataContext;
            this.signalQueue = signalQueue;
            this.logger = logger;
            this.quotesProvider.OnQuotesUpdate += new SymbolDataHasBeenUpdatedNotification(quotesProvider_OnQuotesUpdate);
        }

        public void quotesProvider_OnQuotesUpdate(string symbol)
        {
            if (!DoesSpreadIncludesSymbol(symbol))
                return;

            if (UnfilledOrdersExists())
                return;

            if (PositionExists())
                return;

            double spreadPrice = MakeSellSpread();

            if (spreadPrice == 0)
                return;

            if (spreadPrice >= this.spreadSettings.SellAfterPrice)
            {
                MakeSellSignals(spreadPrice);
                MakeBuySignals(spreadPrice);
            }
        }

        private bool DoesSpreadIncludesSymbol(string symbol)
        {
            return this.leftLeg.Any(i=>i.Symbol.Equals(symbol)) 
                || this.rightLeg.Any(i=>i.Symbol.Equals(symbol));

        }

        private bool PositionExists()
        {
            return PositionExists(this.leftLeg) || PositionExists(this.rightLeg);
        }

        private bool PositionExists(IEnumerable<Strategy> strategies)
        {
            foreach (Strategy s in strategies)
            {
                if (this.tradingData.GetAmount(s) != 0)
                    return true;
            }

            return false;
        }

        private double MakeSellSpread()
        {
            GenericFactory<double> factory =
                new SellSpreadFactory(this.leftLeg, this.rightLeg, this.quotesProvider);

            return factory.Make();
        }

        private void MakeSellSignals(double sellSpreadPrice)
        {
            foreach (Strategy s in this.leftLeg)
            {
                Signal signal = new Signal(s, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Market, sellSpreadPrice, 0, 0);

                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал {2} на отправку заявки на продажу спреда {3}.", DateTime.Now, this.GetType().Name, signal.ToString(), s.ToString()));

                this.signalQueue.Enqueue(signal);
            }
        }

        private void MakeBuySignals(double sellSpreadPrice)
        {
            foreach (Strategy s in this.rightLeg)
            {
                Signal signal = new Signal(s, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, sellSpreadPrice, 0, 0);

                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал {2} на отправку заявки на продажу спреда {3}.", DateTime.Now, this.GetType().Name, signal.ToString(), s.ToString()));

                this.signalQueue.Enqueue(signal);
            }
        }

        private bool UnfilledOrdersExists()
        {
            return UnfilledOrdersExists(this.leftLeg) || UnfilledOrdersExists(this.rightLeg);
        }

        private bool UnfilledOrdersExists(IEnumerable<Strategy> strategies)
        {
            int count = strategies.Count();

            Strategy[] sArray = strategies.ToArray();

            for (int i = 0; i < count; i++)
            {
                if (this.tradingData.UnfilledExists(sArray[i], OrderType.Market))
                    return true;
            }

            return false;
        }
    }
}
