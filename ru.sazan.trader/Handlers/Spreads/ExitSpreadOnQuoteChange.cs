﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data.Spreads;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.Handlers.Spreads
{
    public class ExitSpreadOnQuoteChange
    {
        private IEnumerable<Strategy> leftLeg, rightLeg;
        private OrderBookContext quotesProvider;
        private DataContext tradingData;
        private Logger logger;
        private ObservableQueue<Signal> signalQueue;
        private SpreadSettings spreadSettings;

        public ExitSpreadOnQuoteChange(OrderBookContext quotesProvider,
            IEnumerable<Strategy> leftLeg,
            IEnumerable<Strategy> rightLeg,
            SpreadSettings spreadSettings,
            DataContext dataContext,
            ObservableQueue<Signal> signalQueue,
            Logger logger)
        {
            this.quotesProvider = quotesProvider;
            this.leftLeg = leftLeg;
            this.rightLeg = rightLeg;
            this.spreadSettings = spreadSettings;
            this.tradingData = dataContext;
            this.signalQueue = signalQueue;
            this.logger = logger;
            this.quotesProvider.OnQuotesUpdate += new SymbolDataHasBeenUpdatedNotification(quotesProvider_OnQuotesUpdate);
        }

        public void quotesProvider_OnQuotesUpdate(string symbol)
        {
            if (!DoesSpreadIncludesSymbol(symbol))
                return;

            if (UnfilledOrdersExists())
                return;

            if (!PositionExists())
                return;

            if (ShortPositionExists())
                return;

            double spreadPrice = MakeSellSpread();

            if (spreadPrice == 0)
                return;

            if (spreadPrice >= this.spreadSettings.FairPrice)
            {
                MakeSellSignals(spreadPrice);
                MakeBuySignals(spreadPrice);
            }
        }

        private bool DoesSpreadIncludesSymbol(string symbol)
        {
            return this.leftLeg.Any(i=>i.Symbol.Equals(symbol)) 
                || this.rightLeg.Any(i=>i.Symbol.Equals(symbol));

        }

        private bool PositionExists()
        {
            return PositionExists(this.leftLeg) || PositionExists(this.rightLeg);
        }

        private bool PositionExists(IEnumerable<Strategy> strategies)
        {
            int count = strategies.Count();

            Strategy[] sArray = strategies.ToArray();

            for (int i = 0; i < count; i++)
            {
                if (this.tradingData.GetAmount(sArray[i]) != 0)
                    return true;
            }

            return false;
        }

        private double MakeSellSpread()
        {
            GenericFactory<double> factory =
                new SellSpreadFactory(this.leftLeg, this.rightLeg, this.quotesProvider);

            return factory.Make();
        }

        private void MakeSellSignals(double buySpreadPrice)
        {
            foreach (Strategy s in this.leftLeg)
            {
                double amount = Math.Abs(this.tradingData.GetAmount(s));

                Signal signal = new Signal(s, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Market, buySpreadPrice, 0, 0);
                signal.Amount = amount;

                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал {2} на отправку заявки на покупку (exit) спреда {3}.", DateTime.Now, this.GetType().Name, signal.ToString(), s.ToString()));

                this.signalQueue.Enqueue(signal);
            }
        }

        private void MakeBuySignals(double buySpreadPrice)
        {
            foreach (Strategy s in this.rightLeg)
            {
                double amount = Math.Abs(this.tradingData.GetAmount(s));

                Signal signal = new Signal(s, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, buySpreadPrice, 0, 0);
                signal.Amount = amount;

                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал {2} на отправку заявки на покупку (exit) спреда {3}.", DateTime.Now, this.GetType().Name, signal.ToString(), s.ToString()));

                this.signalQueue.Enqueue(signal);
            }
        }

        private bool UnfilledOrdersExists()
        {
            return UnfilledOrdersExists(this.leftLeg) || UnfilledOrdersExists(this.rightLeg);
        }

        private bool UnfilledOrdersExists(IEnumerable<Strategy> strategies)
        {
            int count = strategies.Count();

            Strategy[] sArray = strategies.ToArray();

            for (int i = 0; i < count; i++)
            {
                if (this.tradingData.UnfilledExists(sArray[i], OrderType.Market))
                    return true;
            }

            return false;
        }

        private bool ShortPositionExists()
        {
            double lAmount = GetAmount(this.leftLeg);
            double rAmount = GetAmount(this.rightLeg);

            if (lAmount < 0 || rAmount > 0)
                return true;

            return false;
        }

        private double GetAmount(IEnumerable<Strategy> strategies)
        {
            double result = 0;

            foreach (Strategy s in strategies)
            {
                result += this.tradingData.GetAmount(s);
            }

            return result;
        }

    }
}
