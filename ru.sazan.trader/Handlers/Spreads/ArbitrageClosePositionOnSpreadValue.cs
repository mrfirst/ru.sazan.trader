﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Handlers.Spreads
{
    public class ArbitrageClosePositionOnSpreadValue : AddedItemHandler<SpreadValue>
    {
        private ArbitrageSettings arbitrageSetings;
        private Strategy strategy;
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private Logger logger;
        private bool isLeftLegStrategy;

        public ArbitrageClosePositionOnSpreadValue(ArbitrageSettings arbitrageSettings, Strategy strategy, DataContext tradingData, ObservableQueue<Signal> signalQueue, Logger logger)
            : base(tradingData.Get<ObservableCollection<SpreadValue>>())
        {
            this.arbitrageSetings = arbitrageSettings;
            this.strategy = strategy;
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;

            this.isLeftLegStrategy = arbitrageSettings.LeftLeg.Any(s => s.Id == strategy.Id);
        }

        public override void OnItemAdded(SpreadValue item)
        {
            if (!tradingData.PositionExists(strategy))
                return;

            if (tradingData.UnfilledExists(strategy, OrderType.Market))
                return;

            TradeAction? action = GetAction(item);

            if (action == null)
                return;

            Signal signal = MakeSignal(item, action.Value);
            
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сгенерирован {2}.", DateTime.Now, this.GetType().Name, signal.ToString()));

            signalQueue.Enqueue(signal);
        }

        private Signal MakeSignal(SpreadValue item, TradeAction action)
        {
            StrategyVolumeChangeStep strategyVolumeChangeStep = GetStrategyVolumeChangeStep(this.strategy);

            Signal signal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), action, OrderType.Market, arbitrageSetings.SpreadSettings.FairPrice, 0, 0);

            if (strategyVolumeChangeStep != null)
                signal.Amount = GetAmount(strategyVolumeChangeStep.Amount);

            return signal;
        }

        private TradeAction? GetAction(SpreadValue item)
        {
            if (this.tradingData.HasShortPosition(this.strategy))
            {
                if (arbitrageSetings.SpreadSettings.FairPrice >= item.SellAfterPrice)
                    return TradeAction.Buy;
            }

            if (this.tradingData.HasLongPosition(this.strategy))
            {
                if (arbitrageSetings.SpreadSettings.FairPrice <= item.BuyBeforePrice)
                    return TradeAction.Sell;
            }

            return null;
        }

        private double GetAmount(double step)
        {
            double positionAmount = Math.Abs(tradingData.GetAmount(strategy));

            if (positionAmount < step)
                return positionAmount;
            else
                return step;
        }

        private StrategyVolumeChangeStep GetStrategyVolumeChangeStep(Strategy strategy)
        {
            return this.tradingData.Get<IEnumerable<StrategyVolumeChangeStep>>().FirstOrDefault(s => s.StrategyId == strategy.Id);
        }
    }
}
