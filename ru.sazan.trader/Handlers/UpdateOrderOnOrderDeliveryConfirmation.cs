﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Handlers;

namespace ru.sazan.trader.Handlers
{
    public class UpdateOrderOnOrderDeliveryConfirmation:AddedItemHandler<OrderDeliveryConfirmation>
    {
        private Logger logger;

        public UpdateOrderOnOrderDeliveryConfirmation(DataContext tradingData, Logger logger)
            : base(tradingData.Get<ObservableHashSet<OrderDeliveryConfirmation>>())
        {
            this.logger = logger;
        }

        public override void OnItemAdded(OrderDeliveryConfirmation item)
        {
            if (item.Order.IsDelivered)
                return;

            item.Order.DeliveryDate = item.DateTime;
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, брокер подтвердил получение заявки {2}.", DateTime.Now, this.GetType().Name, item.Order.ToString()));
        }

    }
}
