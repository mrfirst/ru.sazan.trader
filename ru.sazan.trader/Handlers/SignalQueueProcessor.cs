﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Events;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers
{
    public class SignalQueueProcessor:Observer
    {
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private ObservableQueue<Order> orderQueue;
        private TradingSchedule schedule;
        private Logger logger;

        public SignalQueueProcessor() :
            this(SignalQueue.Instance,
             OrderQueue.Instance,
             TradingData.Instance,
             new FortsTradingSchedule(),
             DefaultLogger.Instance) { }

        public SignalQueueProcessor(ObservableQueue<Signal> signalQueue, 
            ObservableQueue<Order> orderQueue, 
            DataContext tradingData, 
            TradingSchedule schedule,
            Logger logger)
        {
            this.signalQueue = signalQueue;
            this.orderQueue = orderQueue;
            this.tradingData = tradingData;
            this.schedule = schedule;
            this.logger = logger;

            this.signalQueue.RegisterObserver(this);
        }

        public void Update()
        {
            Signal signal = this.signalQueue.Dequeue();

            if (!this.schedule.ItIsTimeToTrade(BrokerDateTime.Make(DateTime.Now)))
            {
                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал удален, потому что в настоящий момент торгов нет.", DateTime.Now, this.GetType().Name));
                return;
            }

            IEnumerable<Order> unfilledOrders = this.tradingData.Get<ICollection<Order>>().GetUnfilledOrderJustLikeASignal(signal);

            if (unfilledOrders != null && unfilledOrders.Count() > 0)
            {
                foreach(Order o in unfilledOrders)
                    this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сигнал удален, потому что существуют неисполненные заявки для стратегии {2}.", DateTime.Now, this.GetType().Name, o.ToString()));

                return;
            }

            SaveSignal(signal);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, формирование заявки по сигналу {2}", DateTime.Now, this.GetType().Name, signal.ToString()));

            Order order = new Order(signal);

            this.orderQueue.Enqueue(order);
        }

        private void SaveSignal(Signal signal)
        {
            this.tradingData.Get<ObservableHashSet<Signal>>().Add(signal);
        }
    }
}
