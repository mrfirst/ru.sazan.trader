﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Events;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Handlers
{
    public class UpdatePositionOnTrade:AddedItemHandler<Trade>
    {
        private DataContext tradingData;
        private Logger logger;


        public UpdatePositionOnTrade(DataContext tradingData, Logger logger)
            :base(tradingData.Get<ObservableHashSet<Trade>>())
        {
            this.logger = logger;
            this.tradingData = tradingData;
        }

        public override void OnItemAdded(Trade item)
        {
            if (!StrategyExists(item))
                return;

            if (item.Order.IsFilled)
            {
                if (Amount(GetOrderTrades(item.OrderId)) > item.Order.Amount)
                    this.tradingData.Get<ICollection<Trade>>().Remove(item);

                return;
            }

            if (!PositionExists(item))
            {
                CreatePosition(item);    
            }
            else
            {
                UpdatePosition(item);
            }

            UpdateOrderAmount(item);
        }

        private bool StrategyExists(Trade item)
        {

            if (item.Order == null)
                return false;

            if (item.Order.Signal == null)
                return false;

            if (item.Order.Signal.Strategy == null)
                return false;

            Strategy strategy = item.Order.Signal.Strategy;

            return this.tradingData.Get<IEnumerable<Strategy>>().Any(s => s.Id == strategy.Id
                && s.Description == strategy.Description
                && s.Portfolio == strategy.Portfolio
                && s.Symbol == strategy.Symbol
                && s.Amount == strategy.Amount);
        }

        private bool PositionExists(Trade item)
        {
            return this.tradingData.Get<ICollection<Position>>().Exists(item.Portfolio, item.Symbol);
        }

        private void CreatePosition(Trade item)
        {
            this.tradingData.Get<ObservableHashSet<Position>>().Add(new Position(item.Portfolio, item.Symbol, item.Amount));
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сделкой {2} инициализирована позиция.", DateTime.Now, this.GetType().Name, item.ToString()));
        }

        private void UpdatePosition(Trade item)
        {
            try
            {
                Position position = this.tradingData.Get<IEnumerable<Position>>().Single(p => p.Portfolio == item.Portfolio && p.Symbol == item.Symbol);

                position.Amount += item.Amount;

                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, позиция изменена {2}.", DateTime.Now, this.GetType().Name, position.ToString()));
            }
            catch
            {
                this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, при попытке изменить информацию о позиции {2} произошла ошибка.", DateTime.Now, this.GetType().Name, item.ToString()));
            }
        }

        private void UpdateOrderAmount(Trade item)
        {
            if(!item.Order.IsFilled)
                item.Order.FilledAmount += Math.Abs(item.Amount);
        }

        private double Amount(IEnumerable<Trade> trades)
        {
            return Math.Abs(trades.Sum(t => t.Amount));
        }

        private IEnumerable<Trade> GetOrderTrades(int orderId)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Trade>>().Where(t => t.OrderId == orderId);
            }
            catch
            {
                return null;
            }
        }
    }
}
