﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers
{
    public class MarkPartiallyFilledOrderAsOutdatedOnTick:AddedItemHandler<Tick>
    {
        private Strategy strategy;
        private int outdateSeconds;
        private Logger logger;
        private DataContext tradingData;

        public MarkPartiallyFilledOrderAsOutdatedOnTick(Strategy strategy, int outdateSeconds, DataContext tradingData, Logger logger)
            :base(tradingData.Get<ObservableCollection<Tick>>())
        {
            this.strategy = strategy;
            this.outdateSeconds = outdateSeconds;
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void OnItemAdded(Tick item)
        {
            if (strategy.Symbol != item.Symbol)
                return;

            double amount = this.tradingData.GetAmount(this.strategy);

            if (amount == 0)
                return;

            IEnumerable<Order> filledPartially = this.tradingData.GetFilledPartially(this.strategy, OrderType.Limit);

            if (filledPartially == null)
                return;

            if (filledPartially.Count() == 0)
                return;

            Order o = filledPartially.First();

            if (o.TradeAction == TradeAction.Buy && amount < 0)
                return;

            if (o.TradeAction == TradeAction.Sell && amount > 0)
                return;

            if (CancelOrderRequestExists(o.Id))
            {
                if(!CancelOrderRequestIsOutdated(o.Id))
                    return;
            }

            if (o.DateTime.AddSeconds(this.outdateSeconds) > item.DateTime)
                return;

            string descr = String.Format("Отменить заявку {0}, потому что она не исполнилась полностью в течение {1} секунд", o.ToString(), this.outdateSeconds);
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, {2}", DateTime.Now, this.GetType().Name, descr));
            this.tradingData.Get<ICollection<OrderCancellationRequest>>().Add(new OrderCancellationRequest(o, descr));
        }

        private bool CancelOrderRequestExists(int orderId)
        {
            return this.tradingData.Get<IEnumerable<OrderCancellationRequest>>().Any(o => o.OrderId == orderId);
        }

        private bool CancelOrderRequestIsOutdated(int orderId)
        {
            return this.tradingData.Get<IEnumerable<OrderCancellationRequest>>().Any(o => o.OrderId == orderId
                && o.DateTime < o.DateTime.AddSeconds(this.outdateSeconds * 3));
        }

    }
}
