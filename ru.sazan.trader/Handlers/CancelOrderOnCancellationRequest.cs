﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.Handlers
{
    public class CancelOrderOnCancellationRequest:AddedItemHandler<OrderCancellationRequest>
    {
        private OrderManager manager;
        private Logger logger;

        public CancelOrderOnCancellationRequest(OrderManager manager, DataContext tradingData, Logger logger)
            :base(tradingData.Get<ObservableHashSet<OrderCancellationRequest>>())
        {
            this.manager = manager;
            this.logger = logger;
        }

        public override void OnItemAdded(OrderCancellationRequest item)
        {
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, отправляется запрос на отмену заявки {2}.", DateTime.Now, this.GetType().Name, item.Order.ToString()));
            this.manager.CancelOrder(item.Order);
        }
    }
}
