﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers
{
    public class TrailStopOnTick:AddedItemHandler<Tick>
    {
        private DataContext tradingData;
        private Strategy strategy;
        private Logger logger;

        private Order stopOrder;
        private double stopPrice;
        private MoveOrder moveOrder;

        public TrailStopOnTick(Strategy strategy,
            DataContext tradingData,
            Logger logger)
            : base(tradingData.Get<ObservableCollection<Tick>>())
        {
            this.strategy = strategy;
            this.tradingData = tradingData;
            this.logger = logger;

            this.stopOrder = null;
            this.moveOrder = null;
            this.stopPrice = 0;
        }

        public override void OnItemAdded(Tick item)
        {
            if (!this.tradingData.PositionExists(this.strategy))
                return;

            if (item.Symbol != this.strategy.Symbol)
                return;

            this.stopOrder = null;
            this.moveOrder = null;

            this.stopOrder = this.tradingData.GetUnfilled(this.strategy, OrderType.Stop).Last();

            if (stopOrder == null)
                return;

            if (TickPriceWasNotBecomeCloserToProfit(item.Price))
                return;

            this.stopPrice = MakeNextStopPrice(item.Price);

            this.moveOrder =
                new MoveOrder(this.stopOrder,
                    this.stopPrice,
                    BrokerDateTime.Make(DateTime.Now),
                    String.Format("Необходимо подтянуть стоп к значению {0}", this.stopPrice));

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, создан запрос на сдвиг заявки {2}", 
                DateTime.Now, 
                this.GetType().Name, 
                this.moveOrder.ToString()));

            this.tradingData.Get<ObservableHashSet<MoveOrder>>().Add(this.moveOrder);
        }

        private bool TickPriceWasNotBecomeCloserToProfit(double price)
        {
            if (this.stopOrder.TradeAction == TradeAction.Buy)
                return this.stopOrder.Signal.Price <= price;

            return this.stopOrder.Signal.Price >= price;
        }

        private double MakeNextStopPrice(double currentPrice)
        {
            if (this.stopOrder.TradeAction == TradeAction.Buy)
                return this.stopOrder.Stop - (this.stopOrder.Signal.Price - currentPrice);

            return this.stopOrder.Stop + (currentPrice - this.stopOrder.Signal.Price);
        }
    }
}
