﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers.StopLoss
{
    public class StrategyStopLossByPointsOnTick:StrategyStopLossOnItemAddedBase<Tick>
    {
        private double openPrice;
        private Trade openTrade;

        public StrategyStopLossByPointsOnTick(Strategy strategy, bool measureFromSignalPrice = false)
            : this(strategy,
            TradingData.Instance,
            SignalQueue.Instance,
            DefaultLogger.Instance, measureFromSignalPrice) { }

        public StrategyStopLossByPointsOnTick(Strategy strategy,
            DataContext tradingData,
            ObservableQueue<Signal> signalQueue,
            Logger logger,
            bool measureFromSignalPrice = false)
            : base(strategy, tradingData, signalQueue, logger, measureFromSignalPrice) { }

        private void UpdateOpenPrice(Trade item)
        {
            if (this.measureFromSignalPrice)
                this.openPrice = item.Order.Signal.Price;
            else
                this.openPrice = item.Price;
        }

        private void UpdateOpenTrade()
        {
            this.openTrade = this.tradingData.GetPositionOpenTrade(this.strategy);
        }

        private void UpdateCloseSignalAmountWith(double amount)
        {
            this.signal.Amount = Math.Abs(amount);
        }

        private void MakeSignalToBuyWith(double price)
        {
            this.signal =
                new Signal(this.strategy,
                BrokerDateTime.Make(DateTime.Now),
                TradeAction.Buy,
                OrderType.Market,
                price,
                0,
                0);
        }

        private void MakeSignalToSellWith(double price)
        {
            this.signal =
                new Signal(this.strategy,
                    BrokerDateTime.Make(DateTime.Now),
                    TradeAction.Sell,
                    OrderType.Market,
                    price,
                    0,
                    0);
        }

        public override void MakeSignal(Tick item, double positionAmount)
        {
            UpdateOpenTrade();

            if (this.openTrade == null)
                return;

            UpdateOpenPrice(this.openTrade);

            if (positionAmount > 0 && this.openPrice - this.spSettings.Points >= item.Price)
                MakeSignalToSellWith(item.Price);
            else if (positionAmount < 0 && this.openPrice + this.spSettings.Points <= item.Price)
                MakeSignalToBuyWith(item.Price);

            if(this.signal != null)
                UpdateCloseSignalAmountWith(positionAmount);
        }
    }
}
