﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers.StopLoss
{
    public class PlaceStrategyStopLossByPointsOnTrade:StrategyStopLossOnItemAddedBase<Trade>
    {
        public PlaceStrategyStopLossByPointsOnTrade(Strategy strategy, bool measureFromSignalPrice = false)
            :base(strategy, 
            TradingData.Instance,
            SignalQueue.Instance,
            DefaultLogger.Instance,
            measureFromSignalPrice) { }

        public PlaceStrategyStopLossByPointsOnTrade(Strategy strategy,
            DataContext tradingData,
            ObservableQueue<Signal> signalQueue,
            Logger logger,
            bool measureFromSignalPrice = false)
            :base(strategy, tradingData, signalQueue, logger, measureFromSignalPrice)
        {
        }

        public override void MakeSignal(Trade item, double positionAmount)
        {
            if (positionAmount.HasOppositeSignWith(item.Amount))
                return;

            if (item.Amount > 0)
                this.signal =
                    new Signal(this.strategy,
                        BrokerDateTime.Make(DateTime.Now),
                        TradeAction.Sell,
                        OrderType.Stop,
                        item.Price,
                        this.measureFromSignalPrice ? (item.Order.Signal.Price - this.spSettings.Points) : (item.Price - this.spSettings.Points),
                        0);
            else
                this.signal =
                    new Signal(this.strategy,
                        BrokerDateTime.Make(DateTime.Now),
                        TradeAction.Buy,
                        OrderType.Stop,
                        item.Price,
                        this.measureFromSignalPrice ? (item.Order.Signal.Price + this.spSettings.Points) : (item.Price + this.spSettings.Points),
                        0);

            if(this.signal != null)
                this.signal.Amount = Math.Abs(positionAmount);

        }
    }
}