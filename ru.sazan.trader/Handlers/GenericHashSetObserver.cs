﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;
using ru.sazan.trader.Events;

namespace ru.sazan.trader.Handlers
{
    public abstract class GenericHashSetObserver<T>:GenericObserver<T>
    {
        protected ObservableHashSetFactory tradingData;

        public GenericHashSetObserver(ObservableHashSetFactory tradingData)
        {
            this.tradingData = tradingData;
            this.tradingData.Make<T>().RegisterObserver(this);
        }

        public abstract void Update(T item);
    }
}
