﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader
{
    public interface Logger
    {
        void Log(string message);
    }
}
