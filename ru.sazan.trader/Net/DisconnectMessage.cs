using System;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.Net
{
	public class DisconnectMessage : Message
	{
		private Guid sourceAddress;
		private Guid destinationAddress;

		public DisconnectMessage(Guid srcAddr, Guid dstAddr)
		{
			this.sourceAddress = srcAddr;
			this.destinationAddress = dstAddr;
		}

		public byte[] GetSourceAddressBytes()
		{
			return this.sourceAddress.ToByteArray();
		}

		public byte[] GetDestinationAddressBytes()
		{
			return this.destinationAddress.ToByteArray();
		}

		public byte[] GetContentBytes()
		{
			string content = "disconnect";

			return content.AsByteArray();
		}
	}
}
