using System;

namespace ru.sazan.trader.Net
{
	public interface Message
	{
		byte[] GetSourceAddressBytes();
		byte[] GetDestinationAddressBytes();
		byte[] GetContentBytes();
	}
}
