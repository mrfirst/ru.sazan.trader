﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Net
{
    public interface Connector
    {
        void Connect();
        void Disconnect();
        bool IsConnected { get; }
    }
}
