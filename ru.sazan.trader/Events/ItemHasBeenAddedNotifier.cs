﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Events
{
    public delegate void ItemHasBeenAddedNotification<T>(T item);

    public interface ItemHasBeenAddedNotifier<T>
    {
        event ItemHasBeenAddedNotification<T> OnItemAdded;
    }
}
