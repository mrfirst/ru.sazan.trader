﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Events
{
    public interface GenericObserver<T>
    {
        void Update(T item);
    }
}
